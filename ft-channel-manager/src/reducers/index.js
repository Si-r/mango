import { combineReducers } from 'redux'
import common from './common'
import collapse from "./collapse";

export default combineReducers({
  common,
  collapse
})