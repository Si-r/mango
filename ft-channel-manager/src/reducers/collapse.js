const initialState = {
  toggleStateA: false,
  breakWidth: 1024,
  windowWidth: 0,
  currentPage: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'setWindowWidth':
      state = {
        ...state,
        windowWidth: action.payload
      }
      break;
    case 'setBreakWidth':
      state = {
        ...state,
        breakWidth: action.payload
      }
      break;
    case 'setToggleStateA':
      state = {
        ...state,
        toggleStateA: action.payload
      }
      break;
    case 'setCurrentPage':
      state = {
        ...state,
        currentPage: action.payload
      }
      break;
    default:
      return state
  }

  return state
}