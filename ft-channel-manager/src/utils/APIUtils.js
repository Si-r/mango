import React from "react";
import { API_BASE_URL, POLL_LIST_SIZE, ACCESS_TOKEN } from '../constants';
import { Redirect } from 'react-router-dom';

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })
    
    if(localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
    .then(response => 
        response.json().then(json => {
            if(!response.ok) {
                return Promise.reject(json);
            }
            return json;
        })
    );
};

const requestMultipartFormData = (options) => {
    const headers = new Headers()
    
    if(localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
    .then(response => 
        response.json().then(json => {
            if(!response.ok) {
                return Promise.reject(json);
            }
            return json;
        })
    );
};

const convertJsonToFormData = (object) => {
    const data = new FormData();
    Object.keys(object)
    .map((keys) => {
        return data.append(keys, object[keys]);
    })
    
    return data;
}

export function getAllPolls(page, size) {
    page = page || 0;
    size = size || POLL_LIST_SIZE;

    return request({
        url: API_BASE_URL + "/polls?page=" + page + "&size=" + size,
        method: 'GET'
    });
}

export function getCurrentUser() {
    if(!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.");
    }

    return request({
        url: API_BASE_URL + "/user/me",
        method: 'GET'
    });
}

export function logout(){
    localStorage.clear();
    return <Redirect to="/signin" />
}

export function login(loginRequest) {
    return request({
        url: API_BASE_URL + "/user/sign-in",
        method: 'POST',
        body: JSON.stringify(loginRequest)
    });
}

export function signup(signupRequest) {
    return requestMultipartFormData({
        url: API_BASE_URL + "/user/",
        method: 'POST',
        body: convertJsonToFormData(signupRequest)
    });
}