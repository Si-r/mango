import React, { Component } from "react";
import PropTypes from "prop-types";
import { MDBPagination, MDBPageItem, MDBPageNav } from "mdbreact";

class TableUtils extends Component {
  state = {};

  render() {
    return (
      <div className="overflow-auto mt-3 mb-3 ">
        <div>
          <div className="float-left">Showing 1 to 5 of 57 entries</div>
          <div className="float-right">
            <MDBPagination circle>
              <MDBPageItem disabled>
                <MDBPageNav className="page-link">
                  <span>First</span>
                </MDBPageNav>
              </MDBPageItem>
              <MDBPageItem disabled>
                <MDBPageNav className="page-link" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                  <span className="sr-only">Previous</span>
                </MDBPageNav>
              </MDBPageItem>
              <MDBPageItem active>
                <MDBPageNav className="page-link">
                  1 <span className="sr-only">(current)</span>
                </MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">2</MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">3</MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">4</MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">5</MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">&raquo;</MDBPageNav>
              </MDBPageItem>
              <MDBPageItem>
                <MDBPageNav className="page-link">Last</MDBPageNav>
              </MDBPageItem>
            </MDBPagination>
          </div>
        </div>

        <table className="table-utils w-100">
          <thead>
            <tr>
              {Object.keys(this.props.header)
                .filter(keys => !keys.includes("_id"))
                .map((keys, n) => {
                  return (
                    <th key={n} className={`text-center w-${this.props.columnSize[keys]}`}>
                      {this.props.header[keys]}
                    </th>
                  );
                })}
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((object, i) => {
              return (
                <tr key={i}>
                  {Object.keys(object)
                    .filter(keys => !keys.includes("_id"))
                    .map((keys, n) => {
                      if (keys === "status") {
                        const c = object[keys].toLowerCase();
                        if (c === "booked") {
                          return <td className="green-text" key={n}>Booked</td>;
                        } else if (c === "cancelled") {
                          return <td className="red-text" key={n}>Cancelled</td>;
                        } else if (c === "modified") {
                          return <td className="orange-text" key={n}>Modified</td>;
                        }
                      }

                      if(keys === 'totalAmount') {
                        return <td className="text-right" key={n}>{object[keys]}</td>;
                      }

                      if(keys === 'checkIn') {
                        return <td className="text-center" key={n}>{object[keys]}</td>;
                      }

                      if(keys === 'checkOut') {
                        return <td className="text-center" key={n}>{object[keys]}</td>;
                      }

                      if(keys === 'createDate') {
                        return <td className="text-center" key={n}>{object[keys]}</td>;
                      }

                      return <td key={n}>{object[keys]}</td>;
                    })}
                </tr>
              );
            })}
          </tbody>
        </table>
        {this.props.children}
      </div>
    );
  }
}

TableUtils.propTypes = {
  name: PropTypes.string,
  header: PropTypes.object,
  data: PropTypes.array,
  columnSize: PropTypes.object
};

TableUtils.defaultProps = {
  name: "",
  header: {},
  data: [],
  columnSize: {}
};

export default TableUtils;
