import React, { Component } from "react";
import PropTypes from "prop-types";
import { MDBBadge } from "mdbreact";

class TableRoom extends Component {
  state = {
    collapseID: "collapse3"
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ""
    }));

  findSubItem = (object, id, key) => {
    const { collapseID } = this.state;
    return (
      <tr
        key={`tr-sub-${key}`}
        className={`tr-channel collapse ${collapseID === `sub-item-${id}` &&
          "show"}`}
        ref={this.setRef}
      >
        {Object.keys(object)
          .filter(keyname => !keyname.includes("_id"))
          .map((keyName, n) => {
            return (
              <td colSpan={n === 1 ? 1 : 2} key={`td-main-${n}`}>
                {object[keyName]}
              </td>
            );
          })}
      </tr>
    );
  };

  childrenItem = () => {
    const item =
      this.props.plan.length > 0 &&
      this.props.plan.map(function(object, i) {
        return (
          <tbody key={`tbody-${i}`}>
            <tr>
              <td>
                <MDBBadge
                  onClick={this.toggleCollapse(`sub-item-${i}`)}
                  color="grey darken-2 channel-chliren pt-2 pb-2"
                >
                  {
                    this.props.planChannel.filter(
                      channel => channel.plan_id === object.plan_id
                    ).length
                  }
                </MDBBadge>
              </td>
              {Object.keys(object)
                .filter(keyname => !keyname.includes("_id"))
                .map((keyName, n) => {
                  return <td key={`td-main-${n}`}>{object[keyName]}</td>;
                })}
            </tr>
            {this.props.planChannel.length > 0 &&
              this.props.planChannel
                .filter(channel => channel.plan_id === object.plan_id)
                .map(function(channel, m) {
                  // console.log(channel)
                  return this.findSubItem(channel, i, m);
                }, this)}
          </tbody>
        );
      }, this);
    return item;
  };

  render() {
    return (
      <div className="mb-3">
        <table className="table-item w-100">
          {this.props.children}
          <thead>
            <tr>
              <th colSpan="3">{this.props.room.name}</th>
            </tr>
          </thead>
          {this.childrenItem()}
        </table>
      </div>
    );
  }
}

TableRoom.propTypes = {
  name: PropTypes.string,
  colNum: PropTypes.number,
  room: PropTypes.object,
  plan: PropTypes.array,
  planChannel: PropTypes.array
};

TableRoom.defaultProps = {
  name: "",
  colNum: 0,
  room: {},
  plan: [],
  planChannel: []
};

export default TableRoom;
