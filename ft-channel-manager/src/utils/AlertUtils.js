import React from "react";
import { MDBNotification } from "mdbreact";

class Notification {
    static error(title, message) {
        return (
            <MDBNotification
                show
                fade
                autohide={3000}
                iconClassName="text-danger"
                title={title}
                message={message}
                text="just now"
            />
        )
    }

    static success(title, message) {
        return (
            <MDBNotification
                show
                fade
                autohide={3000}
                iconClassName="text-primary"
                title={title}
                message={message}
                text="just now"
            />
        )
    }
}

export default Notification;