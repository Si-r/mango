import React from "react";
import { DefaultLayout } from "../views/layouts/welcome";
import {
  DashBoard,
  RoomRateIndex,
  InventoryIndex,
  ChannelsIndex,
  ReportsIndex,
  ReservationsIndex,
  ProfileIndex
} from "../views/back-office/";
import { BackOfficeLayout } from "../views/layouts/back-office";
import { SignIn, SignUp } from "../views/welcome/";
import { Redirect } from "react-router-dom";
import { logout } from "../utils/APIUtils";

const web = [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/signin" />
  },
  {
    path: "/signin",
    exact: true,
    middleware: false,
    layout: DefaultLayout,
    component: SignIn
  },
  {
    path: "/signup",
    exact: true,
    middleware: false,
    layout: DefaultLayout,
    component: SignUp
  },
  {
    path: "/back-office",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: () => <Redirect to="/back-office/dashboard" />
  },
  {
    path: "/back-office/dashboard",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: DashBoard
  },
  {
    path: "/back-office/logout",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: () => logout()
  },
  {
    path: "/back-office/my-account",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: () => <Redirect to="/back-office/my-account/property-setting" />
  },
  {
    path: "/back-office/my-account/property-setting",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: ProfileIndex
  },
  {
    path: "/back-office/my-account/user-setting",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: ProfileIndex
  },
  {
    path: "/back-office/inventory",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: InventoryIndex
  },
  {
    path: "/back-office/room-rate-setup",
    middleware: true,
    layout: BackOfficeLayout,
    component: RoomRateIndex
  },
  {
    path: "/back-office/reservations",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: ReservationsIndex
  },
  {
    path: "/back-office/Reports",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: ReportsIndex
  },
  {
    path: "/back-office/channels",
    exact: true,
    middleware: true,
    layout: BackOfficeLayout,
    component: ChannelsIndex
  }
];

export default web;
