export const MOCK_RESERVATION = [
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "Cancelled",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "Modified",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "booked",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "Cancelled",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  },
  {
    reservationId: "1234567",
    guestName: "John",
    checkIn: "1/12/62",
    checkOut: "1/12/62",
    status: "Cancelled",
    createDate: "1/12/62",
    channel: "Agoda",
    totalAmount: "1000.00"
  }
];

export const MOCK_RESERVATION_HEADER = {
  reservationId: "Reservation ID",
  guestName: "Guest Name",
  checkIn: "Check-in",
  checkOut: "Check-out",
  status: "Status",
  createDate: "Create Date",
  channel: "Channel",
  totalAmount: "Total Amount (Baht)"
};

export const MOCK_RESERVATION_HEADER_WITH = {
  reservationId: "15",
  guestName: "20",
  checkIn: "10",
  checkOut: "10",
  status: "10",
  createDate: "10",
  channel: "10",
  totalAmount: "15"
};
