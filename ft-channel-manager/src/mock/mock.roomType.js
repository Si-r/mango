export const MOCK_ROOM_NEW = [
  {
    room_id: 1,
    name: "(1) Superior Room",
    onActice: true
  },{
    room_id: 2,
    name: "(2) Deluxe Room",
    onActice: true
  },{
    room_id: 3,
    name: "(3) Garden Villa",
    onActice: true
  },{
    room_id: 4,
    name: "(4) Deluxe Garden Villa",
    onActice: true
  }
]

export const MOCK_PLAN = [
  {
    room_id: 1,
    plan_id: 1,
    name: "Value Added ((1) Superior Room)",
    val: "",
  },
  {
    room_id: 1,
    plan_id: 2,
    name: "DEFAULT 2018-07-04 ((1) Superior Room - NRF)",
    val: "Rate amounts derived from (1) Superior Room / Value Added ((1) Superior Room)",
  },
  {
    room_id: 2,
    plan_id: 3,
    name: "Value Added ((2) Deluxe Room)",
    val: "",
  },
  {
    room_id: 2,
    plan_id: 4,
    name: "DEFAULT 2018-07-04 ((2) Deluxe Room - NRF)",
    val: "Rate amounts derived from (2) Deluxe Room / Value Added ((2) Deluxe Room)",
  },
  {
    room_id: 3,
    plan_id: 5,
    name: "Value Added ((3) Garden Villa)",
    val: "",
  },
  {
    room_id: 4,
    plan_id: 6,
    name: "Value Added ((4) Deluxe Garden Villa)",
    val: "",
  }
]

export const MOCK_PLAN_CHANNEL = [
    {
      plan_id: 1,
      name: "Ch11",
      val: "Rate amounts derived from (1) Superior Room / DEFAULT 2018-07-04 ((1) Superior Room - NRF)"
    },
    {
      plan_id: 1,
      name: "Ch12",
      val: "Rate amounts derived from (1) Superior Room / DEFAULT 2018-07-04 ((1) Superior Room - NRF)"
    },
    {
      plan_id: 2,
      name: "Ch21",
      val: "Rate amounts derived from (1) Superior Room / DEFAULT 2018-07-04 ((1) Superior Room - NRF)"
    }
]