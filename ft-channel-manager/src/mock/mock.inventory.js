export const MOCK_INVENTORY = [
    {
        roomName: 'Standard with Bathtub',
        availability: [
            {
                date: 'Tue, 02 Jan 2018',
                number: '2'
            },
            {
                date: 'Wed, 03 Jan 2018',
                number: '2'
            }
        ],
        plan: [
            {
                name: 'Standard with Bathtub',
                channel: [
                    {
                        name: 'Channel 1',
                        cost: [
                            {
                                date: 'Tue, 02 Jan 2018',
                                price: '1500'
                            }
                        ]
                    }
                ]
            }
        ]
    }
]