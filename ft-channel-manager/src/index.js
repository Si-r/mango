import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "./dist/css/mdb.css"
// import "mdbreact/dist/css/mdb.css";
import "./dist/css/index.css";
import App from "./App";

import rootReducer from './reducers'
import './i18n'

import registerServiceWorker from './registerServiceWorker';

const store = createStore(rootReducer)

const MyApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

ReactDOM.render(<MyApp />, document.getElementById('root'));

registerServiceWorker();