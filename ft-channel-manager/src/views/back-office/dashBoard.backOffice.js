import React from 'react';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardHeader,
  MDBIcon,
  MDBProgress,
  MDBListGroup,
  MDBListGroupItem,
  MDBBadge
} from 'mdbreact';

const DV6 = () => {
  return (
    <MDBContainer fluid>
      <MDBRow>
        <MDBCol md='6' className='mb-4'>
          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>Property status</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>Disabled Channel connections 
                <MDBBadge color="warning-color-dark float-right mt-1" pill>2</MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Disabled Channel Room Rates
                <MDBBadge color="warning-color-dark float-right mt-1" pill>2</MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Channel connections experiencing delayed updates
                <MDBBadge color="success-color-dark float-right mt-1" pill>2</MDBBadge>
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>

          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>Channel Status</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>Booking.com
                <MDBBadge color="success-color-dark float-right mt-1" pill>
                  <MDBIcon icon="check"></MDBIcon>
                </MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Expedia
                <MDBBadge color="danger-color-dark float-right mt-1" pill>
                  Connection To Channel Disabled
                </MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Agoda
                <MDBBadge color="warning-color-dark float-right mt-1" pill>
                  Channel Room Rates Disabled
                </MDBBadge>
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>
        </MDBCol>

        <MDBCol md='6' className='mb-4'>
          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>System Alerts</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>No Current System Alerts
                <MDBBadge color="warning-color-dark float-right mt-1" pill>2</MDBBadge>
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>

          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>Announcements</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>No current announcements
                <MDBBadge color="warning-color-dark float-right mt-1" pill>2</MDBBadge>
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>

          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>Reservations Received</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>New Reservations today
                <MDBBadge color="success-color-dark float-right mt-1">2</MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Reservations in the last 30 days
                <MDBBadge color="grey darken-2 float-right mt-1">180</MDBBadge>
              </MDBListGroupItem>
              <MDBListGroupItem>Reservations in total
                <MDBBadge color="grey darken-2 float-right mt-1">180</MDBBadge>
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>

          <MDBCard className='mb-4'>
            <MDBCardHeader color='grey lighten-3 dark-grey-text'>Top Channels, Last 30 Days</MDBCardHeader>
            <MDBListGroup>
              <MDBListGroupItem>Booking.com
                <MDBProgress
                  className='mb-2 mt-1'
                  value={100}
                  barClassName='danger-color-dark'
                />
              </MDBListGroupItem>
              <MDBListGroupItem>Expedia
                <MDBProgress
                  className='mb-2 mt-1'
                  value={50}
                  barClassName='warning-color-dark'
                />
              </MDBListGroupItem>
              <MDBListGroupItem>Agoda
                <MDBProgress
                  className='mb-2 mt-1'
                  value={10}
                  barClassName='success-color-dark'
                />
              </MDBListGroupItem>
            </MDBListGroup>
          </MDBCard>
        </MDBCol>

      </MDBRow>
    </MDBContainer>
  );
};

export default DV6;
