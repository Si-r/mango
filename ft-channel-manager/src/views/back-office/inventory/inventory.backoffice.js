import React, { Component } from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBSelect,
  MDBIcon,
  MDBDatePicker,
  MDBBtn,
  MDBBadge
} from "mdbreact";

class Inventory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calendar: []
    };
  }
  componentWillMount() {}

  componentDidMount() {
    this.calendarHeader();
    // this.setDefaultData()
    // this.onHandleOption()
  }

  calendarHeader() {
    const cal = [];
    for (let i = 19; i <= 27; i++) {
      cal.push(i);
    }

    this.setState({
      calendar: cal
    });
  }

  getPickerValue = value => {
    console.log(value);
  };

  render() {
    return (
      <div>
        <MDBContainer className="pt-3 pl-3 pr-3 pb-0" fluid>
          <MDBRow>
            <MDBCol xl="3" md="6">
              <MDBSelect
                className="mb-2 mt-2"
                outline
                multiple
                options={this.state.options}
                getValue={this.onChangeHandleData}
                selected="Choose Room type"
                selectAll
              />
            </MDBCol>
            <MDBCol xl="3" md="6">
              <MDBSelect
                className="mb-2 mt-2"
                outline
                multiple
                options={this.state.options_2}
                getValue={this.onChangeHandleSub}
                selected="Choose Rate plans"
                selectAll
              />
            </MDBCol>
            <MDBCol xl="3" md="6">
              <div className="input-group mb-2 mt-2">
                <div className="input-group-prepend">
                  <div className="input-group-text ">
                    {" "}
                    <MDBIcon icon="search" />
                  </div>
                </div>
                <input
                  value={this.state.textSearch}
                  onChange={this.handleChange}
                  name="textSearch"
                  type="text"
                  className="form-control py-0"
                  id="inlineFormInputGroup"
                  placeholder="Search"
                />
              </div>
            </MDBCol>
          </MDBRow>
        </MDBContainer>

        <MDBContainer className="overflow-auto" fluid>
          <MDBRow className="calendar-header">
            <MDBCol size="4">
              <MDBRow>
                <MDBCol size="8">
                  <MDBRow>
                    <MDBCol size="6">To</MDBCol>
                    <MDBCol size="6">Form</MDBCol>
                  </MDBRow>
                  <MDBRow>
                    <MDBCol size="6">
                      <MDBDatePicker
                        className="m-0"
                        getValue={this.getPickerValue}
                      />
                    </MDBCol>
                    <MDBCol size="6">
                      <MDBDatePicker
                        className="m-0"
                        getValue={this.getPickerValue}
                      />
                    </MDBCol>
                  </MDBRow>
                </MDBCol>
                <MDBCol size="4">
                  <MDBBtn className="pt-2 pb-2 mt-2 mb-2" color="indigo">
                    Search
                  </MDBBtn>
                </MDBCol>
              </MDBRow>
            </MDBCol>
            <MDBCol size="8">
              <table className="w-100">
                <tbody>
                  <tr>
                    <td>
                      <span>
                        <MDBIcon icon="angle-double-left" />
                      </span>
                    </td>
                    <td>
                      <span aria-hidden="true">
                        <MDBIcon icon="angle-left" />
                      </span>
                    </td>
                    <td>
                      <MDBRow>
                        <MDBCol className="small text-center" size="12">
                          Dec 2019
                        </MDBCol>
                        <MDBCol className="small text-center" size="12">
                          web
                        </MDBCol>
                        <MDBCol
                          className="text-center font-weight-bold"
                          size="1 2"
                        >
                          18
                        </MDBCol>
                      </MDBRow>
                    </td>
                    {this.state.calendar.map((val, n) => {
                      return (
                        <td key={n}>
                          <MDBRow>
                            <MDBCol className="small text-center" size="12">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </MDBCol>
                            <MDBCol className="small text-center" size="12">
                              web
                            </MDBCol>
                            <MDBCol
                              className="text-center font-weight-bold"
                              size="12"
                            >
                              {val}
                            </MDBCol>
                          </MDBRow>
                        </td>
                      );
                    })}
                    <td>
                      <MDBIcon icon="angle-double-right" />
                    </td>
                    <td>
                      <MDBIcon icon="angle-right" />
                    </td>
                  </tr>
                </tbody>
              </table>
            </MDBCol>
          </MDBRow>

          <MDBRow>
            {['Standard with Bathtub', 'Deluxe Room', 'Deluxe Room'].map((table, t) => {
              return <table className="inventory-table w-100 mr-3 ml-3 mt-3" key={t}>
              <thead>
                <tr>
                  <th className="w-7">Channel</th>
                  <th className="w-20">{table}</th>
                  <th className="w-8">AVAUKABILTY</th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">0</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                  <th className="text-center"><MDBBadge className="white" color="light">2</MDBBadge></th>
                </tr>
              </thead>
              <tbody>
                {[1,2,0,0,0,1,2].map((val, n) => {
                  return <tr key={n}>
                  <td className="text-center"><MDBBadge className="white" color="light-blue darken-3">{val}</MDBBadge></td>
                  <td>{table}</td>
                  <td><b>RATES</b></td>
                  <td className="text-center">1{val}00</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                  <td className="text-center">1500</td>
                </tr>
                })}
              </tbody>
            </table>
            })}
            
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default Inventory;
