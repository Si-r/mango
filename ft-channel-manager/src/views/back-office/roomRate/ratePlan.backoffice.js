import React, { Component } from 'react';
import { MDBCol, MDBSelect, MDBRow, MDBIcon, MDBBtn, MDBContainer } from 'mdbreact';
import Table from '../../../utils/TableUtils';
import { MOCK_ROOM_NEW, MOCK_PLAN, MOCK_PLAN_CHANNEL } from '../../../mock/mock.roomType';

class RatePlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      options_2: [],
      room: [],
      plan: [],
      planChannel: [],
      items: [],
      textSearch: ""
    }
  }
  componentWillMount(){
    this.setDefaultData()
    this.onHandleOption()
    this.onHandleOption_2()
  }

  componentDidMount() {
    // this.setDefaultData()
    // this.onHandleOption()
  }

  setDefaultData(e) {
    this.setState({
      room: MOCK_ROOM_NEW,
      plan: MOCK_PLAN,
      planChannel: MOCK_PLAN_CHANNEL
    }, e)
  }

  onHandleOption() {
    const items = MOCK_ROOM_NEW.map(function(object) {
      return {text: object.name , value: object.room_id.toString()}
    })

    this.setState({
      options : items
    })
  }

  onHandleOption_2() {
    const items = MOCK_PLAN.map(function(object) {
      return {text: object.name , value: object.plan_id.toString()}
    })

    this.setState({
      options_2 : items
    })
  }

  onLoadTable(room, key) {
    let planItems = this.state.plan.filter(plan => parseInt(plan.room_id) === parseInt(room.room_id))
    let planChannelItems = this.state.planChannel.filter(planChannel => {
      for (let i = 0; i < planItems.length; i++) {
        let bool = parseInt(planChannel.plan_id) === parseInt(planItems[i].plan_id)
        if (bool) {
          return bool
        }
      }
    })

    return <Table
      key={`table-${key}`}
      colNum={2}
      room={room}
      plan={planItems}
      planChannel={planChannelItems}
      />
  }

  onChangeHandleData = v => {
    this.setDefaultData(() => {
      const filter = this.state.room.filter(item => {
        let bool;
        for (let i = 0; i < v.length; i++) {
          bool = parseInt(item.room_id) === parseInt(v[i])
          if (bool) {
            return bool
          }
        }
      }, this)
  
      this.setState({
        room: filter
      })
    })
  }

  onChangeHandleSub = v => {
    this.setState({
      plan: MOCK_PLAN
    }, () => {
      const filter = this.state.plan.filter(item => {
        let bool;
        for (let i = 0; i < v.length; i++) {
          bool = parseInt(item.plan_id) === parseInt(v[i])
          if (bool) {
            return bool
          }
        }
      }, this)
  
      this.setState({
        plan: filter
      })
    })
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value }, this.onHandleSearch);
  };

  onHandleSearch = () => {
    const { textSearch } = this.state;
    const lowercasedTextSearch = textSearch.toLowerCase();
    const filteredData = MOCK_ROOM_NEW.filter(item => {
      return Object.keys(item).some(key =>
        typeof item[key] === "string" && item[key].toLowerCase().includes(lowercasedTextSearch)
      );
    });

    this.setState({
      room : filteredData
    })
  }

  onHandleReOrder = () => {
    this.setState({
      plan : []
    })
  }

  onHandleClear = () => {
    this.setState({
      plan : MOCK_PLAN
    })
  }


  render() {
    return (
      <MDBContainer className="pl-0 pr-0" fluid>
        <MDBRow className="mb-2">
          <MDBCol xl="2" md="6">
            <MDBSelect
              className='mb-2 mt-2'
              outline
              multiple
              options={this.state.options}
              getValue={this.onChangeHandleData}
              selected="Choose your option"
              label="Choose Room type"
              selectAll
            />
          </MDBCol>
          <MDBCol xl="2" md="6">
            <MDBSelect
              className='mb-2 mt-2'
              outline
              multiple
              options={this.state.options_2}
              getValue={this.onChangeHandleSub}
              selected="Choose your option"
              label="Choose Rate plans"
              selectAll
            />
          </MDBCol>
          <MDBCol xl="2" md="6">
            <div className='input-group mb-2 mt-2'>
              <div className='input-group-prepend'>
                <div className='input-group-text '> <MDBIcon icon="search" />
                </div>
              </div>
              <input
                value={this.state.textSearch}
                onChange={this.handleChange}
                name='textSearch'
                type='text'
                className='form-control py-0'
                id='inlineFormInputGroup'
                placeholder='Search'
              />
             
            </div>
          </MDBCol>
          <MDBCol xl="4" md="6">
            <MDBBtn 
              color="indigo" 
              className="pt-2 pb-2 mt-2 mb-2"
              onClick={this.onHandleReOrder}
            >
              <MDBIcon icon="arrows-alt" className="mr-2"></MDBIcon>
              Re-order
            </MDBBtn>
            <MDBBtn 
              outline 
              color="indigo" 
              className="pt-2 pb-2 mt-2 mb-2"
              onClick={this.onHandleClear}
            >Clear all</MDBBtn>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="12">
            {
              this.state.room.map(function (object, i) {
                return this.onLoadTable(object, i)
              }, this)
            }
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    )
  }
}

export default RatePlan;

// class RatePlan extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

//   render() {
    
//     return (
//       <MDBContainer fluid></MDBContainer>
//     )
//   }
// }

// export default RatePlan;