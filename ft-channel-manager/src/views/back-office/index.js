import DashBoard from './dashBoard.backOffice'
import RoomRateIndex from './roomRate/roomRateIndex.backoffice'
import InventoryIndex from './inventory/inventoryIndex.backoffice'
import ReportsIndex from './reports/reportsIndex.backoffice'
import ReservationsIndex from './reservations/reservationsIndex.backoffice'
import ChannelsIndex from "./channels/channelsIndex.backoffice";
import ProfileIndex from "./profile/profileIndex.backoffice";

export { DashBoard, RoomRateIndex, InventoryIndex, ReportsIndex, ReservationsIndex, ChannelsIndex, ProfileIndex}
