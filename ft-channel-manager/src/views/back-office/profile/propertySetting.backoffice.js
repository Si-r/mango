import React, { Component } from "react";
import {
  MDBCol,
  MDBRow,
  MDBContainer,
  MDBInput,
  MDBBtn,
  MDBInputGroup
} from "mdbreact";
class PropertySetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minimunRate: "",
      updatePeriod: ""
    };
  }
  componentWillMount() {}

  componentDidMount() {}

  onHandleInputNumber = e => {
    let val;
    if (e.target.value > 365) {
      val = 365;
    } else if (e.target.value < 0) {
      val = 0;
    } else {
      val = e.target.value;
    }

    this.setState({
      [e.target.name]: val
    });
  };

  render() {
    return (
      <MDBContainer className="pr-0" fluid>
        <MDBRow>
          <MDBCol size="12">
            <label>Minimum Rate</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="4">
            <MDBInput
              name="minimunRate"
              type="number"
              label="365 days"
              value={this.state.minimunRate}
              onChange={this.onHandleInputNumber}
              outline
            />
          </MDBCol>
          <MDBCol size="8">
            <p>
              *Setting a minimun rate is optional. if uoi enter a value you will
              not be able to set rates via the inventory grid or control panel
              to be iess than this value. this value must be lower
            </p>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Update Period</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="4">
            <MDBInput
              name="updatePeriod"
              type="number"
              label="365 days"
              value={this.state.updatePeriod}
              onChange={this.onHandleInputNumber}
              outline
            />
          </MDBCol>
          <MDBCol size="8">
            <p>
              *Set the maximun number of days that updates are sent to your
              connected channels. the number of day each channels supports can
              be found in the channels tab.
            </p>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Email</label>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="4">
            <MDBInput type="email" label="John@mail.com" outline />
          </MDBCol>
          <MDBCol size="4" middle>
            <MDBBtn className="mt-0" color="success" size="sm">
              Verified
            </MDBBtn>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>
              Property Name <strong className="red-text">*</strong>
            </label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="4">
            <MDBInput type="text" outline />
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Property Address</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="3">
            <div>
              <MDBInputGroup
                containerClassName="mb-3"
                append={<div className="input-group-text  red-text">*</div>}
                inputs={
                  <select className="browser-default custom-select">
                    <option value="0">County</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                }
              />
            </div>
          </MDBCol>
          <MDBCol size="9">
            <MDBInput name="address" type="text" label="Adress" outline />
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="3">
            <MDBInputGroup
              containerClassName="mb-3"
              append={<div className="input-group-text  red-text">*</div>}
              inputs={
                <select className="browser-default custom-select">
                  <option value="0">District</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              }
            />
          </MDBCol>
          <MDBCol size="3">
            <MDBInputGroup
              containerClassName="mb-3"
              append={<div className="input-group-text  red-text">*</div>}
              inputs={
                <select className="browser-default custom-select">
                  <option value="0">Province</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              }
            />
          </MDBCol>
          <MDBCol size="3">
            <MDBInput
              name="postelCode"
              type="text"
              label="Postel Code"
              outline
            />
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Tel</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="3">
            <MDBInput name="tel" type="text" label="081-234-5678" outline />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    );
  }
}

export default PropertySetting;
