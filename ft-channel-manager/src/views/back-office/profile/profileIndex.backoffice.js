import React, { Component } from "react";
import { MDBContainer, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";
import PropertySetting from "./propertySetting.backoffice";
import UserSetting from "./userSetting.backoffice";
import { Switch, Route } from "react-router-dom";

class ProfileIndex extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeItem1: 1
    };
  }

  componentWillMount() {

  }

  toggle = tabsNum => tab => e => {
    if (this.state["activeItem" + tabsNum] !== tab) {
      this.setState({
        ["activeItem" + tabsNum]: tab
      });
    }
  };

  render() {
    const routes = [
      {
        path: "/back-office/my-account/property-setting",
        component: PropertySetting,
        exact: true
      },
      {
        path: "/back-office/my-account/user-setting",
        component: UserSetting,
        exact: true
      }
    ];

    return (
      <MDBContainer fluid>
        {process.env.API_URL}
        <div>
          <MDBNav className="nav-tabs grey lighten-3 dark-grey-text mr-auto">
            <MDBNavItem>
              <MDBNavLink
                exact to="/back-office/my-account/property-setting"
                role="tab"
              >
                Property Setting
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink
                exact to="/back-office/my-account/user-setting"
                role="tab"
              >
                User Setting
              </MDBNavLink>
            </MDBNavItem>
          </MDBNav>
          {/* <div className="nav-tab-right">
              Quick Tour - Room & Rate Setup
          </div> */}
        </div>
        <div className="border border-top-0 p-3 ">
          {/* {this.state.activeItem1 === 1 && <PropertySetting />}
          {this.state.activeItem1 === 2 && <UserSetting />} */}

          <Switch>
            {routes.map((route, i) => (
              <Route
                key={i}
                path={route.path}
                exact={route.exact}
                render={props => (
                  // pass the sub-routes down to keep nesting
                  <route.component {...props} routes={route.routes} />
                )}
              />
            ))}
          </Switch>
        </div>
      </MDBContainer>
    );
  }
}

export default ProfileIndex;
