import React, { Component } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBInput, MDBFormInline } from "mdbreact";

class UserSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: ""
    };
  }

  onClick = nr => () => {
    this.setState({
      radio: nr
    });
  };

  render() {
    return (
      <MDBContainer className="pr-0" fluid>
        <MDBRow>
          <MDBCol size="12">
            <label>Admin Permission</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="4">
            <MDBInput type="email" label="John@mail.com" outline />
          </MDBCol>
          <MDBCol size="4" middle>
            <MDBBtn className="mt-0" color="danger" size="sm">
              Add
            </MDBBtn>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Admin Setting</label>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <MDBInput
              onClick={this.onClick(1)}
              checked={this.state.radio === 1 ? true : false}
              label="Dashboard"
              type="radio"
              id="radio1"
            />
            <MDBInput
              onClick={this.onClick(2)}
              checked={this.state.radio === 2 ? true : false}
              label="Rooms & Rates"
              type="radio"
              id="radio2"
            />
            <MDBInput
              onClick={this.onClick(3)}
              checked={this.state.radio === 3 ? true : false}
              label="Inventory"
              type="radio"
              id="radio3"
            />
            <MDBInput
              onClick={this.onClick(4)}
              checked={this.state.radio === 4 ? true : false}
              label="Reservation"
              type="radio"
              id="radio4"
            />
            <MDBInput
              onClick={this.onClick(5)}
              checked={this.state.radio === 5 ? true : false}
              label="Report"
              type="radio"
              id="radio5"
            />
            <MDBInput
              onClick={this.onClick(6)}
              checked={this.state.radio === 6 ? true : false}
              label="Channel"
              type="radio"
              id="radio6"
            />
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Notification Setting</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="12" middle>
            <MDBFormInline>
              <MDBInput
                onClick={this.onClick('email')}
                checked={this.state.radio === 'email' ? true : false}
                label="E-mail"
                type="radio"
                id="email"
                containerClass="mr-5"
              />
              <MDBInput
                onClick={this.onClick('sms')}
                checked={this.state.radio === 'sms' ? true : false}
                label="SMS"
                type="radio"
                id="sms"
                containerClass="mr-5"
              />
            </MDBFormInline>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="12" middle>
            <MDBFormInline>
              <MDBInput
                onClick={this.onClick('line')}
                checked={this.state.radio === 'line' ? true : false}
                label="Line@"
                type="radio"
                id="line"
                containerClass="mr-5"
              />
              <MDBInput
                onClick={this.onClick('message')}
                checked={this.state.radio === 'message' ? true : false}
                label="Message"
                type="radio"
                id="message"
                containerClass="mr-5"
              />
            </MDBFormInline>
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol size="12">
            <label>Languaghe</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol size="12" middle>
            <MDBFormInline>
              <MDBInput
                onClick={this.onClick('thai')}
                checked={this.state.radio === 'thai' ? true : false}
                label="ไทย"
                type="radio"
                id="thai"
                containerClass="mr-5"
              />
              <MDBInput
                onClick={this.onClick("englist")}
                checked={this.state.radio === 'englist' ? true : false}
                label="Englist"
                type="radio"
                id="englist"
                containerClass="mr-5"
              />
            </MDBFormInline>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    );
  }
}

export default UserSetting;
