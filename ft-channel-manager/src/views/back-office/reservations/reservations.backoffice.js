import React, { Component } from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBInputGroup,
  MDBSelect,
  MDBDatePicker,
  MDBBtn
} from "mdbreact";
import TableUtils from "../../../utils/TableUtils";
import {
  MOCK_RESERVATION,
  MOCK_RESERVATION_HEADER,
  MOCK_RESERVATION_HEADER_WITH
} from "../../../mock/mock.reservations";

class Reservations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [
        {
          text: "Option 1",
          value: "1"
        },
        {
          text: "Option 2",
          value: "2"
        },
        {
          text: "Option 3",
          value: "3"
        }
      ],
      data: [],
      header: {},
      columnSize: {}
    };
  }
  componentWillMount() {}

  componentDidMount() {
    this.setDefaultData();
    // this.onHandleOption()
  }

  setDefaultData() {
    this.setState({
      data: MOCK_RESERVATION,
      header: MOCK_RESERVATION_HEADER,
      columnSize: MOCK_RESERVATION_HEADER_WITH
    });
  }

  getPickerValue = value => {
    console.log(value);
  };

  render() {
    return (
      <MDBContainer className="pl-0 pr-0" fluid>
        <MDBRow>
          <MDBCol md="3">
            <label>Reservation ID</label>
          </MDBCol>
          <MDBCol md="3">
            <label>Guest Name</label>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol className="mb-3" md="3">
            <MDBInputGroup
              prepend={
                <div className="input-group-text">
                  <MDBIcon icon="search" />
                </div>
              }
            />
          </MDBCol>
          <MDBCol className="mb-3" md="3">
            <MDBInputGroup
              prepend={
                <div className="input-group-text">
                  <MDBIcon icon="search" />
                </div>
              }
            />
          </MDBCol>
          <MDBCol className="mb-3" md="3">
            <MDBSelect
              className=""
              outline
              selected="Choose your option"
              label="All Channels"
              selectAll
            />
          </MDBCol>

          <MDBCol className="mb-3" md="3">
            <MDBSelect
              outline
              options={this.state.options}
              selected="Choose your option"
              label="All Booking Status"
            />
          </MDBCol>
        </MDBRow>

        <MDBRow>
          <MDBCol className="mb-3" md="3">
            <label>From</label>
            <MDBRow>
              <MDBCol md="12">
                <MDBDatePicker className="m-0" getValue={this.getPickerValue} />
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol className="mb-3" md="3">
            <label>To</label>
            <MDBRow>
              <MDBCol md="12">
                <MDBDatePicker className="m-0" getValue={this.getPickerValue} />
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol style={{textAlign: "end"}} className="mb-3 pt-2" md="6">
            <MDBBtn className="pt-2 pb-2 mt-2 mb-2" color="indigo">Search</MDBBtn>
            <MDBBtn className="pt-2 pb-2 mt-2 mb-2" outline color="indigo">Reset</MDBBtn>
          </MDBCol>
        </MDBRow>

        <TableUtils
          data={this.state.data}
          header={this.state.header}
          columnSize={this.state.columnSize}
        ></TableUtils>
      </MDBContainer>
    );
  }
}

export default Reservations;
