import React, { Component } from 'react';
import { MDBContainer, MDBNav, MDBNavItem, MDBNavLink } from 'mdbreact';
import Reservations from './reservations.backoffice';

class ReservationsIndex extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeItem1: 1
    };
  }

  toggle = tabsNum => tab => e => {
    if (this.state['activeItem' + tabsNum] !== tab) {
      this.setState({
        ['activeItem' + tabsNum]: tab
      });
    }
  };

  render() {
    return (
      <MDBContainer fluid>
        <div>
          <MDBNav className="nav-tabs grey lighten-3 dark-grey-text mr-auto">
            <MDBNavItem>
              <MDBNavLink link active={this.state.activeItem1 === 1} to="#" role="tab" onClick={this.toggle(1)(1)}>Reservations</MDBNavLink>
            </MDBNavItem>
          </MDBNav>
          {/* <div className="nav-tab-right">
              Quick Tour - Room & Rate Setup
          </div> */}
        </div>
        <div className="border border-top-0 p-3 ">
          {this.state.activeItem1 === 1 && <Reservations />}
        </div>
      </MDBContainer>
    )
  }
}

export default ReservationsIndex;