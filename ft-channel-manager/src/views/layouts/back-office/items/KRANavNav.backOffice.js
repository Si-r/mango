import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Link
} from 'react-router-dom'
import {
  MDBCollapse
} from 'mdbreact';

import PropTypes from 'prop-types'

class KRANavNav extends Component {
  state = {
    collapseID: ""
  }

  toggleCollapse = collapseID => () => {
    this.props.setCurrentPage(collapseID)
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ""
    }));
  }

  render() {
    return (
      <li>
        <Link
          className={`collapsible-header Ripple-parent arrow-r ${this.props.currentPage === this.props.id ? "active" : ""}`}
          onClick={this.toggleCollapse(this.props.id)}
          to={this.props.to}
        >
          
          <span className="d-md-inline ml-4 font-size-4">{this.props.name}</span>
        </Link>
        <MDBCollapse id={this.props.id} className="" isOpen={this.state.collapseID}>
          <div className="collapsible-body" style={{ display: 'block' }}>
            {this.props.children}
          </div>
        </MDBCollapse>
      </li>
    )
  }
}

KRANavNav.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  icon: PropTypes.string,
  to: PropTypes.string,
  toggleCollapse: PropTypes.func
}

KRANavNav.defaultProps = {
  name: "",
  id: "",
  icon: "",
  to: "",
  toggleCollapse: () => {}
}

const mapStatetoProps = (state) => {
  return {
    currentPage: state.collapse.currentPage
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
    setCurrentPage: (currentPage) => {
      dispatch({
        type: "setCurrentPage",
        payload: currentPage
      })
    }
  }
}



export default connect(mapStatetoProps, mapDispatchtoProps)(KRANavNav);