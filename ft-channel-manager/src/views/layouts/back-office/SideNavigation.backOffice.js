import React, { Component } from "react";

import { MDBSideNavNav, MDBSideNav, MDBCol } from "mdbreact";
import KRANavNav from "./items/KRANavNav.backOffice";

import logo from "../../../assets/logo.png";
class SideNavigation extends Component {
  state = {
    collapseID: "",
    activeID: "",
    listMenu: [
      {
        id: "Dashboard",
        url: "dashboard"
      },
      {
        id: "Inventory",
        url: "inventory"
      },
      {
        id: "Rooms & rates",
        url: "room-rate-setup"
      },
      {
        id: "Reservations",
        url: "reservations"
      },
      {
        id: "Reports",
        url: "reports"
      },
      {
        id: "Channels",
        url: "channels"
      }
    ]
  };

  render() {
    return (
      <MDBSideNav
        className="side-nav-light"
        fixed
        breakWidth={this.props.breakWidth}
        triggerOpening={this.props.triggerOpening}
        style={{ transition: "padding-left .5s" }}
      >
        <li>
          <div className="logo-wrapper font-weight-bold text-center pt-4">
            <MDBCol size="12">
              <div id="circle">
                <img src={logo} alt="logo mango"></img>
              </div>
            </MDBCol>
            <MDBCol size="12">
            <strong className="align-middle">Channel Manager</strong>
            </MDBCol>
          </div>
        </li>
        <MDBSideNavNav>
          {this.state.listMenu.map(function(object, i) {
            return (
              <KRANavNav
                key={i}
                name={object.id}
                id={object.id}
                to={`/back-office/${object.url}`}
              ></KRANavNav>
            );
          })}
        </MDBSideNavNav>
      </MDBSideNav>
    );
  }
}

export default SideNavigation;
