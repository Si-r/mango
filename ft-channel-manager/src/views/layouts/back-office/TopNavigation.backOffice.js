import React, { Component } from "react";
import { BrowserRouter as Router } from 'react-router-dom';
// import { connect } from "react-redux";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBIcon,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBBadge,
  MDBBreadcrumb,
  MDBBreadcrumbItem
} from 'mdbreact';

class TopNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSplitUrl: (window.location.pathname).split("/").slice(1)
    }
    this.handleToggleClickA = this.handleToggleClickA.bind(this);
  }

  handleToggleClickA() {
    this.props.onSideNavToggleClick();
  }

  render() {
    const navStyle = {
      paddingLeft: this.props.toggle ? '16px' : '240px',
      transition: 'padding-left .3s'
    };

    let currentSplitUrl = this.state.currentSplitUrl
    if (currentSplitUrl.length === 0) {
      currentSplitUrl[0] = "Home"   
    } 

    return (
      <Router>
        <MDBNavbar
          className='flexible-MDBNavbar'
          dark
          expand='md'
          scrolling
          fixed='top'
          style={{ zIndex: 3 }}
        >
          <div
            onClick={this.handleToggleClickA}
            key='sideNavToggleA'
            style={{
              lineHeight: '32px',
              marginleft: '1em',
              verticalAlign: 'middle',
              cursor: 'pointer'
            }}
          >
            <MDBIcon icon='bars' color='white' size='lg' />
          </div>

          <MDBNavbarBrand
            style={navStyle}>
            <MDBBreadcrumb uppercase>
              {
                currentSplitUrl.map((val, num) =>
                  <MDBBreadcrumbItem key={num}>{val}</MDBBreadcrumbItem>
                )
              }
            </MDBBreadcrumb>
          </MDBNavbarBrand>
          <MDBNavbarNav expand='sm' right style={{ flexDirection: 'row' }}>
            <MDBDropdown>
              <MDBDropdownToggle nav caret>
                <MDBBadge color='red' className='mr-2'>
                  3
                </MDBBadge>
                <MDBIcon icon='bell' />
                <span className='d-none d-md-inline'>Notifications</span>
              </MDBDropdownToggle>
              <MDBDropdownMenu right style={{ minWidth: '400px' }}>
                <MDBDropdownItem href='#!'>
                  <MDBIcon icon='money-bill-alt' className='mr-2' />
                  New order received
                  <span className='float-right'>
                    <MDBIcon icon='clock' /> 13 min
                  </span>
                </MDBDropdownItem>
                <MDBDropdownItem href='#!'>
                  <MDBIcon icon='money-bill-alt' className='mr-2' />
                  New order received
                  <span className='float-right'>
                    <MDBIcon icon='clock' /> 33 min
                  </span>
                </MDBDropdownItem>
                <MDBDropdownItem href='#!'>
                  <MDBIcon icon='chart-line' className='mr-2' />
                  Your campaign is about to end
                  <span className='float-right'>
                    <MDBIcon icon='clock' /> 53 min
                  </span>
                </MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
            <MDBNavItem>
              <MDBNavLink to='#'>
                <MDBIcon icon='envelope' />
                <span className='d-none d-md-inline ml-1'>Contact</span>
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to='#'>
                <MDBIcon icon='comments' />
                <span className='d-none d-md-inline ml-1'>Support</span>
              </MDBNavLink>
            </MDBNavItem>
            <MDBDropdown>
              <MDBDropdownToggle nav caret>
                <MDBIcon icon='user' />
                <span className='d-none d-md-inline'>Profile</span>
              </MDBDropdownToggle>
              <MDBDropdownMenu right style={{ minWidth: '200px' }}>
                <MDBDropdownItem href='/back-office/logout'>Log Out</MDBDropdownItem>
                <MDBDropdownItem href='/back-office/my-account'>My Account</MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavbarNav>
        </MDBNavbar>
      </Router>
    );
  }
}

// const mapStatetoProps = (state) => {
//   return {
//     toggleStateA: state.collapse.toggleStateA,
//     breakWidth: state.collapse.breakWidth,
//     windowWidth: state.collapse.windowWidth
//   }
// }

// const mapDispatchtoProps = (dispatch) => {
//   return {
//     setWindowWidth: (withScale) => {
//       dispatch({
//         type: "setWindowWidth",
//         payload: withScale
//       })
//     },
//     setToggleStateA: (toggleStateA) => {
//       dispatch({
//         type: "setToggleStateA",
//         payload: toggleStateA
//       })
//     }
//   }
// }

export default TopNavigation;
