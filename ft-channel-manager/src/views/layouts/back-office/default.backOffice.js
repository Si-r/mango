import React, { Component } from "react";
import { connect } from "react-redux";
import { MDBContainer } from "mdbreact";
import PropTypes from "prop-types";
import TopNavigation from './TopNavigation.backOffice'
import SideNavigation from './SideNavigation.backOffice'

class DefaultLayout extends Component {
  constructor(props) {
    super(props);

    this.toggleSideNav = this.toggleSideNav.bind(this);
    this.handleResize = this.handleResize.bind(this);
  }
  componentDidMount = () => {
    this.handleResize();
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.handleResize);
  }

  toggleSideNav() {
    if (this.props.windowWidth < this.props.breakWidth) {
      this.props.setToggleStateA(!this.props.toggleStateA)
    }
  };

  handleResize() {
    this.props.setWindowWidth(window.innerWidth)
  }

  render() {
    const mainStyle = {
      margin: "0 0",
      paddingTop: "2.8rem",
      paddingLeft:
        this.props.windowWidth > this.props.breakWidth ? "240px" : "0"
    };
    return (
      <div className='DefaultLayout'>
        <div className='light-blue-skin'>
          <SideNavigation
            breakWidth={this.props.breakWidth}
            style={{ transition: 'all .3s' }}
            triggerOpening={this.props.toggleStateA}
          />
        </div>

        <div className='flexible-content light-blue-skin'>
          <TopNavigation
            toggle={this.props.windowWidth < this.props.breakWidth}
            onSideNavToggleClick={() => this.toggleSideNav()}
            routeName={this.props.currentPage}
          />

          <main style={mainStyle}>
            <MDBContainer fluid style={{ height: 2000 }} className="backOffice-container">
              {this.props.children}
            </MDBContainer>
          </main>
        </div>
      </div>
    );
  }
}

DefaultLayout.propTypes = {
  noNavbar: PropTypes.bool,
  noFooter: PropTypes.bool
};

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

const mapStatetoProps = (state) => {
  return {
    toggleStateA: state.collapse.toggleStateA,
    breakWidth: state.collapse.breakWidth,
    windowWidth: state.collapse.windowWidth
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
    setWindowWidth: (withScale) => {
      dispatch({
        type: "setWindowWidth",
        payload: withScale
      })
    },
    setToggleStateA: (toggleStateA) => {
      dispatch({
        type: "setToggleStateA",
        payload: toggleStateA
      })
    }
  }
}

export default connect(mapStatetoProps, mapDispatchtoProps)(DefaultLayout);
