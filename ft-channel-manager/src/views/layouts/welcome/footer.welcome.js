import React, { Component } from "react";
import { MDBFooter, MDBIcon, MDBRow, MDBCol, MDBContainer } from "mdbreact";
import PropTypes from "prop-types";

class Footer extends Component {
  state = {
    toggleStateA: false,
    breakWidth: 1300,
    windowWidth: 0
  }
  render() {
    return (
      <MDBFooter color="indigo" className="font-small mt-4 pt-0">
        <MDBContainer>
          <MDBRow className="pt-5 mb-3 text-center d-flex justify-content-center">
            <MDBCol md="2" className="b-3">
              <h6 className="title font-weight-bold">
                <a href="#!">About us</a>
              </h6>
            </MDBCol>
            <MDBCol md="2" className="b-3">
              <h6 className="title font-weight-bold">
                <a href="#!">Products</a>
              </h6>
            </MDBCol>
            <MDBCol md="2" className="b-3">
              <h6 className="title font-weight-bold">
                <a href="#!">Awards</a>
              </h6>
            </MDBCol>
            <MDBCol md="2" className="b-3">
              <h6 className="title font-weight-bold">
                <a href="#!">Help</a>
              </h6>
            </MDBCol>
            <MDBCol md="2" className="b-3">
              <h6 className="title font-weight-bold">
                <a href="#!">Contact</a>
              </h6>
            </MDBCol>
          </MDBRow>
          <hr className="rgba-white-light" style={{ margin: "0 15%" }} />
          <MDBRow className="d-flex text-center justify-content-center mb-md-0 mb-4">
            <MDBCol md="8" sm="12" className="mt-5">
              <p style={{ lineHeight: "1.7rem" }}>
                Sed ut perspiciatis unde omnis iste natus error sit
                voluptatem accusantium doloremque laudantium, totam rem
                aperiam, eaque ipsa quae ab illo inventore veritatis et
                quasi architecto beatae vitae dicta sunt explicabo. Nemo
                enim ipsam voluptatem quia voluptas sit aspernatur aut odit
                aut fugit, sed quia consequuntur.
                  </p>
            </MDBCol>
          </MDBRow>
          <hr
            className="clearfix d-md-none rgba-white-light"
            style={{ margin: "10% 15% 5%" }}
          />
          <MDBRow className="pb-3">
            <MDBCol md="12">
              <div className="mb-5 flex-center">
                <a href="#!" className="fb-ic">
                  <MDBIcon
                    brand
                    icon="facebook"
                    size="lg"
                    className="white-text mr-md-4"
                  />
                </a>
                <a href="#!" className="tw-ic">
                  <MDBIcon
                    brand
                    icon="twitter"
                    size="lg"
                    className="white-text mr-md-4"
                  />
                </a>
                <a href="#!" className="gplus-ic">
                  <MDBIcon
                    brand
                    icon="google-plus"
                    size="lg"
                    className="white-text mr-md-4"
                  />
                </a>
                <a href="#!" className="li-ic">
                  <MDBIcon
                    brand
                    icon="linkedin"
                    size="lg"
                    className="white-text mr-md-4"
                  />
                </a>
                <a href="#!" className="ins-ic">
                  <MDBIcon
                    brand
                    icon="instagram"
                    size="lg"
                    className="white-text mr-md-4"
                  />
                </a>
                <a href="#!" className="pin-ic">
                  <MDBIcon
                    brand
                    icon="pinterest"
                    size="lg"
                    className="white-text"
                  />
                </a>
              </div>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
        <div className="footer-copyright text-center py-3">
          <MDBContainer fluid>
            &copy; {new Date().getFullYear()} Copyright:{" "}
            <a href="!#"> MDBootstrap.com </a>
          </MDBContainer>
        </div>
      </MDBFooter>
    )
  }
}

Footer.propTypes = {
  noNavbar: PropTypes.bool,
  noFooter: PropTypes.bool
};

Footer.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default Footer;