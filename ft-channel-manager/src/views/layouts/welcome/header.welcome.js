import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
  MDBCol,
  MDBContainer
} from "mdbreact";
import PropTypes from "prop-types";
import { Collapse } from "@material-ui/core";

class HeaderLayout extends Component {
  state = {};

  render() {
    return (
      <MDBNavbar color="info-color" dark expand="md">
        <MDBContainer className="krapao-header">
        <MDBCol size="12 p-0" >
          <MDBCol size="5" xl="2" lg="2" md="3"  sm="5" className="float-left" >
            <MDBNavbarBrand href="/">
              <strong className="white-text">E-Commerce</strong>
            </MDBNavbarBrand>
          </MDBCol>

          <MDBCol size="7" xl="3" lg="4" md="5" sm="6" className="float-right p-0" >
            <MDBCollapse isOpen={true} className="float-right nav-krapao">
              <MDBNavbarNav right>
                <MDBNavItem className="">
                  <MDBNavLink className="" to="#!">
                    <MDBIcon icon="cog" className="mr-1" />
                    ตะก้าสินค้า
                  </MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                  <MDBNavLink className="" to="#!">
                    <MDBIcon icon="cog" className="mr-1" />
                    ลงชื่อเข้าใช้
                  </MDBNavLink>
                </MDBNavItem>
              </MDBNavbarNav>
            </MDBCollapse>
          </MDBCol>

          <MDBCol xl="7" lg="6" md="4" sm="12"  className="float-right">
            <div className="input-group form-sm">
              <div className="input-group-prepend">
                <span className="input-group-text " id="basic-text1">
                  <MDBIcon icon="search" className="text-grey" />
                </span>
              </div>
              <input className="form-control " type="text" placeholder="Search" aria-label="Search" />
            </div>
          </MDBCol>
          </MDBCol>
        </MDBContainer>
      </MDBNavbar>
    );
  }
}

HeaderLayout.propTypes = {
  noNavbar: PropTypes.bool,
  noFooter: PropTypes.bool
};

HeaderLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default HeaderLayout;
