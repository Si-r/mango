import React, { Component } from "react"
import PropTypes from "prop-types";
import { getCurrentUser } from "../../../utils/APIUtils";
import { ACCESS_TOKEN } from "../../../constants";
import { Redirect } from "react-router-dom";

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      currentUser: null,
      loading: false,
      redirect: false
    };

    this.loadCurrentlyLoggedInUser = this.loadCurrentlyLoggedInUser.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  loadCurrentlyLoggedInUser() {
    this.setState({
      loading: true
    });

    getCurrentUser()
      .then(response => {
        this.setState({
          currentUser: response,
          authenticated: true,
          loading: false
        });
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  }

  handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    this.setState({
      authenticated: false,
      currentUser: null
    });

    Notification.success("You're safely logged out!");
  }

  componentDidMount() {
    if(localStorage.getItem(ACCESS_TOKEN)) {
      this.setState({
        redirect : true
      })
    }
  }

  render() {
    return (
      <div className='flyout'>
        {this.state.redirect && <Redirect to='/back-office/dashboard' />}
        <main style={{minHeight: '50vh', marginTop: '7px' }}>
          {this.props.children}
        </main>
      </div>
    )
  }
}

DefaultLayout.propTypes = {
  noNavbar: PropTypes.bool,
  noFooter: PropTypes.bool
}

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
}

export default DefaultLayout