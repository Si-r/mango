import React, { Component } from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBBtn,
  MDBCardBody,
  MDBInput,
  MDBModalFooter
} from "mdbreact";

import { translate } from 'react-i18next'
import { login } from "../../utils/APIUtils";
import { ACCESS_TOKEN } from "../../constants";
import Notification from "../../utils/AlertUtils";
import { Redirect } from "react-router-dom";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isValidated: false,
      noti: [],
      username: '',
      password: '',
      login : false
    }

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // this.handleEmailChange = this.handleEmailChange.bind(this);

  handleInput = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit(event) {
    const form = {
      username: this.state.username,
      password: this.state.password
    }

    event.preventDefault();
    event.target.className += " was-validated";
    if (event.target.checkValidity()) {
      login(form)
        .then(response => {
          localStorage.setItem(ACCESS_TOKEN, response.data.jwt);

          if(localStorage.getItem(ACCESS_TOKEN)) {
            this.setState({
              login: true
            })
          }
          
        }).catch(error => {
          if (error.status === 404) {
            this.handleNoti(Notification.error(
              'Error 404',
              error.message || 'Sorry! Something went wrong. Please try again!',
            ));
          } else {
            this.handleNoti(Notification.error(
              'Error 404',
              error.message || 'Sorry! Something went wrong. Please try again!',
            ));
          }
        });
      this.setState({ isValidated: true });
    } else {
      this.setState({ isValidated: false });
    }
  }

  handleNoti = (value) => {
    this.setState({
      noti: [...this.state.noti, value]
    })
  }

  render() {
    const { t } = this.props

    return (
      <MDBContainer>
        <form
          ref={this.props.form}
          onSubmit={this.handleSubmit}
          className="needs-validation login-form"
          noValidate
        >
          <MDBRow className="d-flex justify-content-center align-items-center">
            <MDBCol md='10' lg='6' xl='5' sm='12' className='mt-5 mx-auto'>
              <MDBCard>
                <div className="header pt-3 grey lighten-2">
                  <MDBRow className="d-flex justify-content-start">
                    <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5">
                      {t('SignIn')}
                    </h3>
                  </MDBRow>
                </div>
                <MDBCardBody>
                  <MDBInput
                    id='username'
                    icon='user'
                    iconClass='grey-text'
                    label={t('Your username')}
                    value={this.state.username}
                    onChange={this.handleInput}
                    required
                  >
                    <div className="ml-5 invalid-feedback">{t('Invalid login usernamr')}</div>
                  </MDBInput>

                  <MDBInput
                    id='password'
                    type='password'
                    icon='lock'
                    iconClass='grey-text'
                    label={t('Your password')}
                    value={this.state.password}
                    onChange={this.handleInput}
                    required
                  >
                    <div className="ml-5 invalid-feedback">{t('Invalid login password')}</div>
                  </MDBInput>
                  <MDBCol size="9" className='mx-auto'>
                    <div className='text-center'>
                      <MDBBtn type="submit" color='deep-orange'>{t('SignIn')}</MDBBtn>
                    </div>
                  </MDBCol>

                </MDBCardBody>
                <MDBModalFooter>
                  <div className="font-weight-light">
                    <p>Not a member? <a href={`/signup`} className="blue-text ml-1">Sign Up</a></p>
                    <p>Forgot Password?</p>
                  </div>
                </MDBModalFooter>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </form>

        {this.state.noti.length > 0 &&
          <MDBContainer
            style={{
              width: "auto",
              position: "fixed",
              top: "10px",
              right: "10px",
              zIndex: 9999
            }}
          >
            {
              this.state.noti.map(function (object, i) {
                return <div key={i}>
                  {object}
                </div>
              })
            }
          </MDBContainer>
        }

        {this.state.login && <Redirect to="/back-office/dashboard" />}
      </MDBContainer>
    );
  }
}

export default translate()(SignIn);
