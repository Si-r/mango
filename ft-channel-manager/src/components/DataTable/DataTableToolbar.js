import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { MDBIcon } from 'mdbreact';

class DataTableToolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  useToolbarStyles = () => makeStyles(theme => ({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
    title: {
      flex: '1 1 100%',
    },
  }));

  render() {
    const classes = this.useToolbarStyles();

    const {numSelected} = this.props;

    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography className={classes.title} color="inherit" variant="subtitle1">
            {numSelected} selected
              </Typography>
        ) : (
            <Typography className={classes.title} variant="h6" id="tableTitle">
              Nutrition
              </Typography>
          )}

        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="delete">
              <MDBIcon icon="trash-alt" />
            </IconButton>
          </Tooltip>
        ) : (
            <Tooltip title="Filter list">
              <IconButton aria-label="filter list">
                <MDBIcon icon="filter"/>
              </IconButton>
            </Tooltip>
          )}
      </Toolbar>
    )
  }
}

DataTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

DataTableToolbar.defaultProps = {
  numSelected: 0
}

export default DataTableToolbar;
export { DataTableToolbar };

