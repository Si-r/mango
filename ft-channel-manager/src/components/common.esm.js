import React from 'react';
import classNames from 'classnames';

var NavLink = function NavLink(props) {
  var _useState = useState({}),
      _useState2 = _slicedToArray(_useState, 2),
      cursorPos = _useState2[0],
      setCursorPos = _useState2[1];

  var handleClick = function handleClick(e) {
    if (!props.disabled) {
      e.stopPropagation(); // Waves - Get Cursor Position

      var _cursorPos = {
        top: e.clientY,
        left: e.clientX,
        time: Date.now()
      };
      setCursorPos(_cursorPos);
    }
  };

  var children = props.children,
      className = props.className,
      disabled = props.disabled,
      active = props.active,
      to = props.to,
      attributes = _objectWithoutProperties(props, ["children", "className", "disabled", "active", "to"]);

  var classes = classNames('nav-link', disabled ? 'disabled' : 'Ripple-parent', active && 'active', className);
  return React.createElement(NavLink$1, _extends({
    "data-test": "nav-link",
    className: classes,
    onMouseUp: handleClick,
    onTouchStart: handleClick,
    to: to
  }, attributes), children, props.disabled ? false : React.createElement(Waves, {
    cursorPos: cursorPos
  }));
};

NavLink.propTypes = {
  children: propTypes.node,
  className: propTypes.string,
  disabled: propTypes.bool,
  to: propTypes.string,
  active: propTypes.bool
};
NavLink.defaultProps = {
  active: false,
  className: '',
  disabled: false
};

export {NavLink as CMNavLink}
