import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/* eslint-disable */
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { KeyboardDatePicker as UIDatePicker } from '@material-ui/pickers';
import moment from 'moment';

import './DatePicker.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { MDBIcon } from 'mdbreact';

class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selectedDate: props.value || props.valueDefault,
      muiTheme: createMuiTheme({
        ...props.theme,
        typography: {
          useNextVariants: true
        },
        palette : {
          primary: {
            main: '#4285f4',
            dark: '#4285f4'
          },
          
        },
        shadows: 0
      })
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.getValue &&
      prevState.selectedDate !== this.state.selectedDate
    ) {
      this.props.getValue(this.state.selectedDate);
    }

    if (this.props.value !== prevProps.value) {
      this.setState({ selectedDate: this.props.value });
    }

    if (prevProps.theme !== this.props.theme) {
      this.setState({ muiTheme: createMuiTheme(this.props.theme) });
    }
  }

  handleDateChange = date => {
    this.setState({ selectedDate: date ? date._d : this.props.value });
  };

  render() {
    const {
      icon,
      placeholder,
      label,
      theme,
      allowKeyboardControl,
      animateYearScrolling,
      autoOk,
      cancelLabel,
      clearable,
      clearLabel,
      disableFuture,
      disablePast,
      emptyLabel,
      initialFocusedDate,
      InputAdornmentProps,
      invalidDateMessage,
      invalidLabel,
      keyboard,
      keyboardIcon,
      leftArrowIcon,
      mask,
      maxDate,
      maxDateMessage,
      minDate,
      minDateMessage,
      okLabel,
      rightArrowIcon,
      showTodayButton,
      TextFieldComponent,
      todayLabel,
      locale,
      format,
      className,
      getValue,
      value,
      valueDefault,
      tag: Tag,
      ...attributes
    } = this.props;

    const classes = classNames('md-form-2', className);

    return (
      <Tag data-test='date-picker' className={classes}>

        {icon ? (
          <MDBIcon 
          icon={icon}
          size='2x' 
          className={`mt-2 prefix ${this.state.isOpen ? 'active' : ''}`}></MDBIcon>
        ): ''}
        
        <MuiThemeProvider theme={this.state.muiTheme}>
          <MuiPickersUtilsProvider
            locale={locale}
            moment={moment}
            utils={MomentUtils}
          >
            <UIDatePicker
              onFocus={() => {this.setState({ isOpen: true })}} 
              onBlur={() => {this.setState({ isOpen: false })}} 
              onOpen={() => {this.setState({ isOpen: true })}}
              onClose={() => {this.setState({ isOpen: false })}}
              placeholder={placeholder}
              label={label}
              className='col-md-12'
              style={{marginLeft: '2.5rem', width: 'calc(100% - 2.5rem)'}}
              {...attributes}
              allowKeyboardControl={allowKeyboardControl}
              animateYearScrolling={animateYearScrolling}
              autoOk={autoOk}
              cancelLabel={cancelLabel}
              clearable={clearable}
              clearLabel={clearLabel}
              disableFuture={disableFuture}
              disablePast={disablePast}
              emptyLabel={emptyLabel}
              initialFocusedDate={initialFocusedDate}
              InputAdornmentProps={InputAdornmentProps}
              invalidDateMessage={invalidDateMessage}
              invalidLabel={invalidLabel}
              keyboard={keyboard}
              keyboardIcon={keyboardIcon}
              leftArrowIcon={leftArrowIcon}
              mask={mask}
              maxDate={maxDate}
              maxDateMessage={maxDateMessage}
              minDate={minDate}
              minDateMessage={minDateMessage}
              okLabel={okLabel}
              rightArrowIcon={rightArrowIcon}
              showTodayButton={showTodayButton}
              TextFieldComponent={TextFieldComponent}
              todayLabel={todayLabel}
              format={format || "YYYY/MM/DD"}
              value={this.state.selectedDate}
              onChange={this.handleDateChange}
            />
          </MuiPickersUtilsProvider>
        </MuiThemeProvider>
      </Tag>
    );
  }
}

DatePicker.propTypes = {
  icon: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  theme: PropTypes.object,
  allowKeyboardControl: PropTypes.bool,
  animateYearScrolling: PropTypes.bool,
  autoOk: PropTypes.bool,
  cancelLabel: PropTypes.node,
  clearable: PropTypes.bool,
  clearLabel: PropTypes.node,
  disableFuture: PropTypes.object,
  disablePast: PropTypes.bool,
  emptyLabel: PropTypes.string,
  initialFocusedDate: PropTypes.string,
  InputAdornmentProps: PropTypes.object,
  invalidDateMessage: PropTypes.node,
  invalidLabel: PropTypes.string,
  keyboard: PropTypes.bool,
  keyboardIcon: PropTypes.node,
  leftArrowIcon: PropTypes.node,
  mask: PropTypes.any,
  maxDate: PropTypes.string,
  maxDateMessage: PropTypes.node,
  minDate: PropTypes.string,
  minDateMessage: PropTypes.node,
  okLabel: PropTypes.node,
  rightArrowIcon: PropTypes.node,
  showTodayButton: PropTypes.bool,
  TextFieldComponent: PropTypes.string,
  todayLabel: PropTypes.string,
  locale: PropTypes.string,
  format: PropTypes.string,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
  getValue: PropTypes.func,
  value: PropTypes.instanceOf(Date),
  valueDefault: PropTypes.instanceOf(Date)
};

DatePicker.defaultProps = {
  icon: '',
  placeholder: '',
  label: '',
  theme: {},
  tag: 'div',
  value: null,
  valueDefault: new Date(),
  getValue: () => {}
};

export default DatePicker;
export { DatePicker };
