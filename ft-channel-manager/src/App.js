import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Routes from "./routes/web";

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          {Routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={props => {
                  return (
                    <route.layout {...props}>
                      <route.component {...props} />
                    </route.layout>
                  );
                }}
              />
            );
          })}
        </BrowserRouter>

       
      </div>
    );
  }
}

export default App;
