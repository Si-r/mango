package com.cm.channelmanageapi;

import java.util.Base64;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

class Proof1 {

    private String privateData = "iCMF+I+fLBk5hkl4uyDk9RZJp+Vjkvj9";
    private String password = "passwordnaja";
    final private String  algorithm = "PBEWithMD5AndTripleDES";

    @Test
    public void encrypt(){
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(password);
        encryptor.setAlgorithm(algorithm);
        String encryptedText = encryptor.encrypt(privateData);
        String plainText = encryptor.decrypt(privateData);
        System.out.println(String.format("Encrypted: %s", encryptedText));
        System.out.println(String.format("Decrypted: %s", plainText));
    }

    @Test
    public void bcrypt(){
        System.out.println(BCrypt.hashpw(privateData, BCrypt.gensalt()));
    }

    @Test
    public void encoding(){
        String encoded = Base64.getEncoder().encodeToString("passwordnaja".getBytes());
        String decoded = new String(Base64.getDecoder().decode(encoded));
        System.out.println(String.format("Encode: %s", encoded));
        System.out.println(String.format("Decode: %s", decoded));
    }
}