package com.cm.channelmanageapi.config;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import com.cm.channelmanageapi.entity.Permission;
import com.cm.channelmanageapi.entity.User;
import com.cm.channelmanageapi.repository.PermissionRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import com.cm.channelmanageapi.services.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtHandling {

    @Value("${jwt.time.expire}")
    private long JWT_TOKEN_VALIDITY; // hour * min * sec * milli-sec = millesec

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.subject}")
    private String jwtSubject;

    @Value("${jwt.issuer}")
    private String jwtIssuer;

    @Autowired
    private Encryption encryption;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @PostConstruct
    private void decryptSecret(){
        decryptedSecret = encryption.decrypt(secret);
    }

    private String decryptedSecret;

    // generate token for user
    public String generateToken(String username) {
        User user = userRepository.findByUsername(username).get();
        final Map<String, Object> claims = new HashMap<>();
        List<Permission> permissions = permissionRepository.findByEmail(user.getEmail());
        List<String> permissionStrings = new ArrayList<>();
        if(!permissions.isEmpty()){
            permissions.forEach(p -> {
                permissionStrings.add(p.getPermission().toString());
            });
        }
        claims.put("username", username);
        claims.put("permission", permissionStrings);
        return Jwts.builder().setClaims(claims).setIssuer(jwtIssuer).setSubject(jwtSubject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, decryptedSecret).compact();
    }

    //
    public Claims validateToken(final String token) {
        final Claims claims = Jwts.parser().setSigningKey(decryptedSecret).parseClaimsJws(token).getBody();
        return claims;
    }
    
}