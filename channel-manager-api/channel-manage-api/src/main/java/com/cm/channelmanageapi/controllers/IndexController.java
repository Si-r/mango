package com.cm.channelmanageapi.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class IndexController{

    private static final Logger logger = LogManager.getLogger(IndexController.class);

    @GetMapping(value="/index")
    public String getMethodName() {
        try{
            Integer.parseInt("hello");
        }catch(Exception e){
            logger.error("Fail to parse int", e);
        }
        return "hello world!!";
    }

    @GetMapping("favicon.ico")
    void favicon() {}
}