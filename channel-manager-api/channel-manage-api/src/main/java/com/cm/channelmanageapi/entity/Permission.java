package com.cm.channelmanageapi.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.cm.channelmanageapi.utils.EnumBasic.PermissionEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
public class Permission extends BaseEntity{

    public Permission(String email, Property property, PermissionEnum permission, String createdBy){
        this.email = email;
        this.property = property;
        this.permission = permission;
        this.setCreateBy(createdBy);
    }
    
    public Permission(){
    }
    
    @Enumerated(EnumType.STRING)
    private PermissionEnum permission;

    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_id")
    private Property property;

}