package com.cm.channelmanageapi.models;

import java.io.Serializable;
import lombok.Data;

@Data
public class UsernamePassword implements Serializable{

    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
}