package com.cm.channelmanageapi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserIsExistValidator implements ConstraintValidator<UserIsExist, Integer> {

    @Autowired
    UserRepository userRepository;

    public void initialize(UserIsExistValidator constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        try{
            return userRepository.findById(value).isPresent();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}