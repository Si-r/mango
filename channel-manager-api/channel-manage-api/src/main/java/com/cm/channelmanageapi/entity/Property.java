package com.cm.channelmanageapi.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
public class Property extends BaseEntity{
    
    private Integer minimumRate;
    private Integer updatePeriod;
    private String name;
    private String country;
    private String address;
    private String district;
    private String province;
    private Integer postCode;
    private String tel;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private User ownerUser;

    @OneToMany(mappedBy = "property")
    private List<User> users;
}