package com.cm.channelmanageapi.config;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cm.channelmanageapi.utils.Formatter;
import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -7858869558953243875L;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    Formatter format;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,AuthenticationException authException) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String exceptionName = authException.getClass().getSimpleName();
        ResponseCode responseCode = ResponseCode.UN_AUTHORIZED;
        if("InsufficientAuthenticationException".equals(exceptionName)){
            responseCode = ResponseCode.CANNOT_ACCESS;
        }else if("BadCredentialsException".equals(exceptionName)){
            responseCode = ResponseCode.BAD_CREDENTIALS;
        }
        response.getWriter().write(format.getResponse(responseCode));
    }

    public Authentication verifyCredentials(String username, String password) throws Exception{
        try{
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        }catch(DisabledException e){
            throw new Exception("USER_DISABLED", e);
        }catch(BadCredentialsException e){
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}