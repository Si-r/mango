package com.cm.channelmanageapi.utils;

import java.util.Base64;

public class EnumBasic {

    public enum Secret { 
        // encoding()
        ENCRYPION("cGFzc3dvcmRuYWph"); 

        private final String secret;

        Secret(String secret){
            this.secret = secret;
        }
       
        public String value(){
            return new String(Base64.getDecoder().decode(this.secret));
        }
    }

    public enum ResponseCode {
    /**
     * this enum using for define http response, now have 2 types are (message, status) 
     */
    
    SUCCESS("success", "success"),
    IO_FAIL("IO Exception: System IO Fail", "fail"),
    CANNOT_ACCESS("Full authentication is required to access this resource", "fail"),
    BAD_CREDENTIALS("Bad credentials", "fail"),
    UN_AUTHORIZED("unauthorized", "fail"),
    HTTP_MESSAGE_NOT_READABLE_EXCEPTION("can't read request data, please check format or type of request", "fail"),
    HTTP_MEDIA_TYPE_NOT_SUPPORTED_EXCEPTION("content type is not supported", "fail"),
    NUMBER_EXCEPTION("NumberException: fail to cast data about number", "fail");
    
    /************ END Response Code  ************/

       private final String message;
       private final String status;

       ResponseCode(String message, String status){
            this.message = message;
            this.status = status;
       }

       public String getMessage(){
           return this.message;
       }
       public String getStatus(){
           return this.status;
       }
    }

    public enum PermissionEnum {
        DASHBOARD, ROOM_RATE, INVENTORY, RESERVATION, REPORT, CHANNEL
    }
}