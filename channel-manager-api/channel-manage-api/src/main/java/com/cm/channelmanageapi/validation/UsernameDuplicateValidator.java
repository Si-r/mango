package com.cm.channelmanageapi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UsernameDuplicateValidator implements ConstraintValidator<UsernameDuplicate, String> {

    @Autowired
    UserRepository userRepository;

    public void initialize(UsernameDuplicate constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !userRepository.findByUsername(value).isPresent();
    }
}