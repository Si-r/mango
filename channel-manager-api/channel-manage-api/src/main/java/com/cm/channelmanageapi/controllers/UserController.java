package com.cm.channelmanageapi.controllers;

import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;
import java.util.Date;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.cm.channelmanageapi.config.JwtAuthenticationEntryPoint;
import com.cm.channelmanageapi.config.JwtHandling;
import com.cm.channelmanageapi.entity.User;
import com.cm.channelmanageapi.models.ResponseSignIn;
import com.cm.channelmanageapi.models.ResponseUser;
import com.cm.channelmanageapi.models.UsernamePassword;
import com.cm.channelmanageapi.services.UserService;
import com.cm.channelmanageapi.utils.Formatter;
import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import com.cm.channelmanageapi.validation.EmailDuplicate;
import com.cm.channelmanageapi.validation.UserIsExist;
import com.cm.channelmanageapi.validation.UsernameDuplicate;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/user")
@Validated
class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    JwtHandling jwtHandling;

    @Autowired
    Formatter format;

    @Autowired
    UserService userService;

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> postMethodName(@RequestBody UsernamePassword credentials) throws Exception {
        jwtAuthenticationEntryPoint.verifyCredentials(credentials.getUsername(), credentials.getPassword());
        String jwt = jwtHandling.generateToken(credentials.getUsername());
        logger.info("generated token for user: " + credentials.getUsername());
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.SUCCESS, new ResponseSignIn(jwt)));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/")
    public ResponseEntity<?> getMethodName() {
        
        return ResponseEntity.ok("test");
    }

    @PostMapping(value = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> createUser(Principal principle,
            @UsernameDuplicate @NotBlank @Size(min = 4, max = 20) @RequestParam("username") String username,
            @NotBlank @Size(min = 4, max = 20) @RequestParam("password") String password,
            @EmailDuplicate @Email @RequestParam("email") String email)
            throws JsonProcessingException {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setCreateAt(new Date());
        user.setCreateBy(principle != null ? principle.getName() : null);
        user.setUpdateBy(principle != null ? principle.getName() : null);
        User userDatabase = userService.createUser(user);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
        .body(format.getResponse(ResponseCode.SUCCESS, new ResponseUser(userDatabase)));
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> updateUser(Principal principle,
            @UserIsExist @PathVariable(value = "id") Integer id,
            @EmailDuplicate @Email @RequestParam(value = "email", required = false) String email,
            @Size(min = 4, max = 20) @RequestParam(value = "password", required = false) String password)
            throws JsonProcessingException{
                
        User userUpdated = userService.updateUser(id, email, password, principle);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
            .body(format.getResponse(ResponseCode.SUCCESS, new ResponseUser(userUpdated)));
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> updateUser(Principal principle,
            @UserIsExist @PathVariable(value = "id") Integer id)
            throws JsonProcessingException{
        userService.deleteUser(id);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
            .body(format.getResponse(ResponseCode.SUCCESS));
    }
}