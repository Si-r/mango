package com.cm.channelmanageapi.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = HotelProfileIsExistValidator.class)
@Documented
public @interface HotelProfileIsExist {
    String message() default "not found hotel profile.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}