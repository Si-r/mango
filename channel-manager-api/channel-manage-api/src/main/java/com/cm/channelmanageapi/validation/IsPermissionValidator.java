package com.cm.channelmanageapi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cm.channelmanageapi.utils.EnumBasic.PermissionEnum;

public class IsPermissionValidator implements ConstraintValidator<IsPermission, String[]> {


    public void initialize(RoleIsExistValidator constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(String[] value, ConstraintValidatorContext context) {
        try{
            for(String v : value){
                PermissionEnum.valueOf(v);
            }
            return true;
        }catch(Exception e){
            return false;
        }
    }
}