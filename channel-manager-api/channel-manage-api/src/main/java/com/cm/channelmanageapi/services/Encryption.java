package com.cm.channelmanageapi.services;

import com.cm.channelmanageapi.utils.EnumBasic.Secret;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.stereotype.Service;

@Service
public class Encryption{

    final private static String algorithm = "PBEWithMD5AndTripleDES";

    public String encrypt(final String plaintext){
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(Secret.ENCRYPION.value());
        encryptor.setAlgorithm(algorithm);
        return encryptor.encrypt(plaintext);
    }

    public String decrypt(String encrypted){
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(Secret.ENCRYPION.value());
        encryptor.setAlgorithm(algorithm);
        return encryptor.decrypt(encrypted);
    }

}