package com.cm.channelmanageapi.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
abstract public class BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	protected Integer id;

    private Date createAt;

    private String createBy;

    private Date updateAt = new Date();

    private String updateBy;

}