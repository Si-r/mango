package com.cm.channelmanageapi.controllers;

import com.cm.channelmanageapi.config.JwtAuthenticationEntryPoint;
import com.cm.channelmanageapi.config.JwtHandling;
import com.cm.channelmanageapi.entity.HotelProfile;
import com.cm.channelmanageapi.entity.User;
import com.cm.channelmanageapi.models.ResponseHotelProfile;
import com.cm.channelmanageapi.models.ResponseSignIn;
import com.cm.channelmanageapi.models.ResponseUser;
import com.cm.channelmanageapi.models.UsernamePassword;
import com.cm.channelmanageapi.services.HotelProfileService;
import com.cm.channelmanageapi.services.UserService;
import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import com.cm.channelmanageapi.utils.Formatter;
import com.cm.channelmanageapi.validation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.Date;

@RestController
@RequestMapping("/hotelProfile")
@Validated
class HotelProfileController {

    @Autowired
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    JwtHandling jwtHandling;

    @Autowired
    Formatter format;

    @Autowired
    HotelProfileService hotelProfileService;

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> postMethodName(@RequestBody UsernamePassword credentials) throws Exception {
        jwtAuthenticationEntryPoint.verifyCredentials(credentials.getUsername(), credentials.getPassword());
        String jwt = jwtHandling.generateToken(credentials.getUsername());
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.SUCCESS, new ResponseSignIn(jwt)));
    }

    @GetMapping(value = "/")
    public ResponseEntity<?> getMethodName(@RequestParam String param) {
        return ResponseEntity.ok("test");
    }

    @PostMapping(value = "/")
    public ResponseEntity<?> createHotelProfile(Principal principle,
            @NotBlank @Size(max = 100) @RequestParam("name") String name,
            @NotBlank @RequestParam("address1") String address1,
            String address2,String address3,
            @NotBlank @RequestParam("city") String city,
            @NotBlank @RequestParam("state") String state,
            @NotBlank @RequestParam("country") String country,
            @NotBlank @RequestParam("postalcode") String postalcode)
            throws JsonProcessingException {
        HotelProfile hotelProfile = new HotelProfile();
        hotelProfile.setName(name);
        hotelProfile.setAddress1(address1);
        hotelProfile.setAddress2(address2);
        hotelProfile.setAddress3(address3);
        hotelProfile.setCity(city);
        hotelProfile.setCountry(country);
        hotelProfile.setState(state);
        hotelProfile.setPostalcode(postalcode);
        hotelProfile.setCreateAt(new Date());
        hotelProfile.setCreateBy(principle.getName());
        hotelProfile.setUpdateBy(principle.getName());
        HotelProfile hotelDatabase = hotelProfileService.createHotelProfile(hotelProfile);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
            .body(format.getResponse(ResponseCode.SUCCESS, new ResponseHotelProfile(hotelDatabase)));
    }

    @PatchMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> updateHotelProfile(Principal principle,
            @HotelProfileIsExist @PathVariable(value = "id") Integer id,
            @HotelProfileNameDuplicate @NotBlank @Size(max = 100) @RequestParam(value = "name", required = false) String name,
            @NotBlank @RequestParam("address1") String address1,
            String address2,String address3,
            @NotBlank @RequestParam("city") String city,
            @NotBlank @RequestParam("state") String state,
            @NotBlank @RequestParam("country") String country,
            @NotBlank @RequestParam("postalcode") String postalcode)
            throws JsonProcessingException{

        HotelProfile hotelProfileUpdated = hotelProfileService.updateHotelProfile(id, name, address1, address2, address3, city, state, country, postalcode, principle);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
            .body(format.getResponse(ResponseCode.SUCCESS, new ResponseHotelProfile(hotelProfileUpdated)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteHotelProfile(Principal principle,
            @HotelProfileIsExist @PathVariable(value = "id") Integer id)
            throws JsonProcessingException{
        hotelProfileService.deleteHotel(id);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
            .body(format.getResponse(ResponseCode.SUCCESS));
    }
}