package com.cm.channelmanageapi.controllers;

import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import com.cm.channelmanageapi.entity.Property;
import com.cm.channelmanageapi.models.ResponsePropertySetting;
import com.cm.channelmanageapi.services.PropertyService;
import com.cm.channelmanageapi.services.UserService;
import com.cm.channelmanageapi.utils.Formatter;
import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import com.cm.channelmanageapi.validation.EmailDuplicate;
import com.cm.channelmanageapi.validation.IsPermission;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/property")
@Validated
class PropertyController {

    private static final Logger logger = LogManager.getLogger(PropertyController.class);

    @Autowired
    Formatter format;

    @Autowired
    UserService userService;

    @Autowired
    PropertyService propertyService;

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/setting", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> settingProperty(Principal principal,
            @Positive @Max(365) @RequestParam(value = "minimumRate", required = false) Integer minimumRate,
            @Positive @Max(365) @RequestParam(value = "updatePeriod", required = false) Integer updatePeriod,
            @NotBlank @Size(min = 1, max = 255) @RequestParam("name") String name,
            @NotBlank @Size(min = 1, max = 255) @RequestParam("country") String country,
            @Size(min = 1, max = 255) @RequestParam(value = "address", required = false) String address,
            @NotBlank @Size(min = 1, max = 255) @RequestParam("district") String district,
            @NotBlank @Size(min = 1, max = 255) @RequestParam("province") String province,
            @Positive @RequestParam("postCode") Integer postCode,
            @Size(min = 1) @RequestParam(value = "tel", required = false) String tel) throws JsonProcessingException {
        Property property = new Property();
        property.setMinimumRate(minimumRate);
        property.setUpdatePeriod(updatePeriod);
        property.setName(name);
        property.setAddress(address);
        property.setCountry(country);
        property.setDistrict(district);
        property.setProvince(province);
        property.setPostCode(postCode);
        property.setTel(tel);
        property = propertyService.savePropertySetting(property, principal);
        logger.info(String.format("[%s] update '%s' property", principal.getName(), property.getName()));
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
        .body(format.getResponse(ResponseCode.SUCCESS, new ResponsePropertySetting(property)));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/new-admin", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> createAdminUser(Principal principal,
        @EmailDuplicate @Email @RequestParam("email") String email,
        @IsPermission @RequestParam("permissions") String[] permissions
            ) throws JsonProcessingException {
        propertyService.addNewUserInProperty(permissions, email, principal);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
        .body(format.getResponse(ResponseCode.SUCCESS, "Please check inbox email for register"));
    }
}