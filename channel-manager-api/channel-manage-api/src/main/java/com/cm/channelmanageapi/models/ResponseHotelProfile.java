package com.cm.channelmanageapi.models;

import com.cm.channelmanageapi.entity.HotelProfile;
import com.cm.channelmanageapi.entity.User;
import lombok.Data;

import java.util.Date;

@Data
public class ResponseHotelProfile {

    public ResponseHotelProfile(HotelProfile hotelProfile){
        this.name = hotelProfile.getName();
        this.address1 = hotelProfile.getAddress1();
        this.address2 = hotelProfile.getAddress2();
        this.address3 = hotelProfile.getAddress3();
        this.city = hotelProfile.getCity();
        this.state = hotelProfile.getState();
        this.country = hotelProfile.getCountry();
        this.postalcode = hotelProfile.getPostalcode();
        this.createdAt = hotelProfile.getCreateAt();
        this.createdBy = hotelProfile.getCreateBy();
        this.updatedAt = hotelProfile.getUpdateAt();
        this.updateBy = hotelProfile.getUpdateBy();
    }

    private Integer id;
    private String name;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String country;
    private String postalcode;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updateBy;
}