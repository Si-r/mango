package com.cm.channelmanageapi.repository;

import java.util.List;
import java.util.Optional;
import com.cm.channelmanageapi.entity.Permission;
import com.cm.channelmanageapi.entity.Property;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long> {
    Optional<Permission> findById(Integer id);
    List<Permission> findByEmail(String email);
    List<Permission> findByProperty(Property property);
    void deleteById(Integer id);
}