package com.cm.channelmanageapi.models;

import java.util.Date;
import java.util.List;

import com.cm.channelmanageapi.entity.User;
import lombok.Data;

@Data
public class ResponseUser{

    public ResponseUser(User user){
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.emailVerified = user.getEmailVerified();
        this.createdAt = user.getCreateAt();
        this.createdBy = user.getCreateBy();
        this.updatedAt = user.getUpdateAt();
        this.updateBy = user.getUpdateBy();
    }

    private Integer id;
    private String username;
    private String email;
    private Boolean emailVerified;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updateBy;
    private List<String> permission;
}