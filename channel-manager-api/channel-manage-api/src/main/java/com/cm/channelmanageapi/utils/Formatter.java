package com.cm.channelmanageapi.utils;

import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

@Service
public class Formatter {

    /**
     * 
     * @return JSON String response
     */
    public String getResponse(ResponseCode res) {
        return String.format("{\"status\": \"%s\", \"message\": \"%s\"}", res.getStatus(), res.getMessage());
    }

    public String getFailCustomMessageResponse(String message) {
        return String.format("{\"status\": \"fail\", \"message\": \"%s\"}", message);
    }

    public String getResponse(ResponseCode code, Object data) throws JsonProcessingException {
        ObjectMapper obj = new ObjectMapper();
        return String.format("{\"status\": \"%s\", \"data\": %s}", code.getStatus(), obj.writeValueAsString(data));
    }

}