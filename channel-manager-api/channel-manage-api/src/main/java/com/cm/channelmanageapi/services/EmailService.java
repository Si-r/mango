package com.cm.channelmanageapi.services;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
@Service
public class EmailService {

    public boolean isValidEmailAddress(String email) {
        return EmailValidator.getInstance().isValid(email);
    }
}