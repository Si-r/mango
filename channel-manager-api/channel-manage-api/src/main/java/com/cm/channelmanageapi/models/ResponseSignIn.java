package com.cm.channelmanageapi.models;

import lombok.Data;

@Data
public class ResponseSignIn {
    public ResponseSignIn() {
	}
    public ResponseSignIn(String jwt){
        this.jwt = jwt;
    }

	String jwt;
}