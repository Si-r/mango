package com.cm.channelmanageapi.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper=false)
@Data
public class Role extends BaseEntity{

    public Role(Integer id){
        this.id = id;
    }

    public Role(){}

    private String name;

}