package com.cm.channelmanageapi.models;

import java.util.Date;

import com.cm.channelmanageapi.entity.Property;

import lombok.Data;

@Data
public class ResponsePropertySetting {
    public ResponsePropertySetting() {
	}
    public ResponsePropertySetting(Property property){
        this.id = property.getId();
        this.updatePeriod = property.getUpdatePeriod();
        this.name = property.getName();
        this.country = property.getCountry();
        this.address = property.getAddress();
        this.district = property.getDistrict();
        this.province = property.getProvince();
        this.postCode = property.getPostCode();
        this.tel = property.getTel();
        this.createdAt = property.getCreateAt();
        this.createdBy = property.getCreateBy();
        this.updatedAt = property.getUpdateAt();
        this.updatedBy = property.getUpdateBy();
        this.ownerUser = property.getOwnerUser().getUsername();
        this.ownerEmail = property.getOwnerUser().getEmail();
    }

    private Integer id;
    private Integer updatePeriod;
    private String name;
    private String country;
    private String address;
    private String district;
    private String province;
    private Integer postCode;
    private String tel;
    private String createdBy;
    private Date createdAt;
    private String updatedBy;
    private Date updatedAt;
    private String ownerUser;
    private String ownerEmail;
}