package com.cm.channelmanageapi.services;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import com.cm.channelmanageapi.entity.Permission;
import com.cm.channelmanageapi.entity.User;
import com.cm.channelmanageapi.repository.PermissionRepository;
import com.cm.channelmanageapi.repository.RoleRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    Encryption encryption;

    public User createUser(User user){ // Encrypt password first
        user.setPassword(encryption.encrypt(user.getPassword()));
        List<Permission> permissions = permissionRepository.findByEmail(user.getEmail()); // check already allow property
        if(permissions.size() > 0){
            user.setProperty(permissions.get(0).getProperty());
        }
        return userRepository.save(user);
    }

    public User updateUser(Integer id, String email, String password, Principal principal){
        User user = userRepository.findById(id).get();
        if(email != null) {
            user.setEmail(email);
        }
        if(password != null){
            user.setPassword(encryption.encrypt(password));
        }
        user.setUpdateAt(new Date());
        user.setUpdateBy(principal.getName());
        return userRepository.save(user);
    }

    public void deleteUser(Integer id){
        userRepository.deleteById(id);
    }
}