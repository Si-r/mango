package com.cm.channelmanageapi.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.cm.channelmanageapi.entity.Permission;
import com.cm.channelmanageapi.repository.PermissionRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import com.cm.channelmanageapi.services.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    Encryption encription;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<com.cm.channelmanageapi.entity.User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            List<SimpleGrantedAuthority> permission = new ArrayList<SimpleGrantedAuthority>();
            List<Permission> permissionsEntity = permissionRepository.findByEmail(user.get().getEmail());
            if(!permissionsEntity.isEmpty()){
                permissionsEntity.forEach(p -> {
                    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(p.getPermission().toString());
                    permission.add(authority);
                });
            }
            String password = bCryptPasswordEncoder.encode(encription.decrypt(user.get().getPassword()));
			return new User(username, password, permission);
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
    }
}