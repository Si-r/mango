package com.cm.channelmanageapi.services;

import com.cm.channelmanageapi.entity.HotelProfile;
import com.cm.channelmanageapi.repository.HotelProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.security.Principal;
import java.util.Date;

@Service
@Transactional
public class HotelProfileService {

    @Autowired
    HotelProfileRepository hotelRepository;

    public HotelProfile createHotelProfile(HotelProfile hotelProfile){
        return hotelRepository.save(hotelProfile);
    }

    public HotelProfile updateHotelProfile(Integer id, String name, String address1,
                                           String address2, String address3, String city,
                                           String state, String country, String postalcode,Principal principal){
        HotelProfile hotelProfile = hotelRepository.findById(id).get();
        if(name != null) {
            hotelProfile.setName(name);
        }
        if(address1 != null){
            hotelProfile.setAddress1(address1);
        }
        if(address2 != null){
            hotelProfile.setAddress2(address2);
        }
        if(address3 != null){
            hotelProfile.setAddress3(address3);
        }
        if(city != null){
            hotelProfile.setCity(city);
        }
        if(state != null){
            hotelProfile.setState(state);
        }
        if(country != null){
            hotelProfile.setCountry(country);
        }
        if(postalcode != null){
            hotelProfile.setPostalcode(postalcode);
        }
        hotelProfile.setUpdateAt(new Date());
        hotelProfile.setUpdateBy(principal.getName());
        return hotelRepository.save(hotelProfile);
    }

    public void deleteHotel(Integer id){
        hotelRepository.deleteById(id);
    }
}