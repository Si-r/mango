package com.cm.channelmanageapi.validation;

import com.cm.channelmanageapi.repository.HotelProfileRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HotelProfileNameDuplicateValidator implements ConstraintValidator<HotelProfileNameDuplicate, String> {

    @Autowired
    HotelProfileRepository hotelProfileRepository;

    public void initialize(HotelProfileNameDuplicate constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !hotelProfileRepository.findByName(value).isPresent();
    }
}