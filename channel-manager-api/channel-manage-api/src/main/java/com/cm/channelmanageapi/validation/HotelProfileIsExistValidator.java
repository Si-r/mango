package com.cm.channelmanageapi.validation;

import com.cm.channelmanageapi.repository.HotelProfileRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HotelProfileIsExistValidator implements ConstraintValidator<HotelProfileIsExist, Integer> {

    @Autowired
    HotelProfileRepository hotelProfileRepository;

    public void initialize(HotelProfileIsExistValidator constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        try{
            return hotelProfileRepository.findById(value).isPresent();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}