package com.cm.channelmanageapi.repository;

import java.util.Optional;

import com.cm.channelmanageapi.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    Optional<Role> findById(Integer id);
}