package com.cm.channelmanageapi.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailDuplicateValidator.class)
@Documented
public @interface HotelProfileNameDuplicate {
    String message() default "name is duplicated.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}