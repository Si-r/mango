package com.cm.channelmanageapi.controllers;

import java.io.IOException;
import javax.validation.ConstraintViolationException;
import com.cm.channelmanageapi.utils.Formatter;
import com.cm.channelmanageapi.utils.EnumBasic.ResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
class HandleExceptionController {

    @Autowired
    Formatter format;

    @ResponseBody
    @ExceptionHandler(value = NumberFormatException.class)
    public ResponseEntity<?> handleException(NumberFormatException exception){
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.NUMBER_EXCEPTION));
    }

    @ResponseBody
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<?> handleException(HttpMessageNotReadableException exception){
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.HTTP_MESSAGE_NOT_READABLE_EXCEPTION));
    }

    @ResponseBody
    @ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<?> handleException(HttpMediaTypeNotSupportedException exception){
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.HTTP_MEDIA_TYPE_NOT_SUPPORTED_EXCEPTION));
    }

    @ResponseBody
    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<?> handleException(IOException exception){
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getResponse(ResponseCode.HTTP_MEDIA_TYPE_NOT_SUPPORTED_EXCEPTION));
    }

    @ResponseBody
    @ExceptionHandler(value = {
        ConstraintViolationException.class, 
        MissingServletRequestParameterException.class,
        HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<?> handleException(Exception exception){
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getFailCustomMessageResponse(exception.getMessage()));
    }

    @ResponseBody
    @ExceptionHandler(value = IllegalAccessException.class)
    public ResponseEntity<?> handleException(IllegalAccessException exception){
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getFailCustomMessageResponse(exception.getMessage()));
    }

    @ResponseBody
    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException exception){
        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(format.getFailCustomMessageResponse(exception.getMessage()));
    }
}