package com.cm.channelmanageapi.services;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import com.cm.channelmanageapi.entity.Permission;
import com.cm.channelmanageapi.entity.Property;
import com.cm.channelmanageapi.entity.User;
import com.cm.channelmanageapi.repository.PermissionRepository;
import com.cm.channelmanageapi.repository.PropertyRepository;
import com.cm.channelmanageapi.repository.UserRepository;
import com.cm.channelmanageapi.utils.EnumBasic.PermissionEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PropertyService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    PropertyRepository propertyRepository;

    @Autowired
    PermissionRepository permissionRepository;

    public Property savePropertySetting(Property property, Principal principal){
        // 1. check user is already have property ? 
        User user = userRepository.findByUsername(principal.getName()).get();
        boolean isAlreadyHaveProperty = user.getProperty() != null;
        Property response = null;
        if(isAlreadyHaveProperty){ // update property
            Property update = user.getProperty();
            update.setMinimumRate(property.getMinimumRate() != null ? property.getMinimumRate(): update.getMinimumRate());
            update.setUpdatePeriod(property.getUpdatePeriod() != null ? property.getUpdatePeriod(): update.getUpdatePeriod());
            update.setName(property.getName());
            update.setAddress(property.getAddress() != null ? property.getAddress(): update.getAddress());
            update.setCountry(property.getCountry());
            update.setDistrict(property.getDistrict());
            update.setProvince(property.getProvince());
            update.setPostCode(property.getPostCode());
            update.setTel(property.getTel() != null ? property.getTel(): update.getTel());
            update.setUpdateBy(user.getUsername());
            update.setUpdateAt(new Date());
            response = propertyRepository.save(update);
        }else{ // create new property and update user
            property.setOwnerUser(user);
            property.setCreateBy(user.getUsername());
            property.setCreateAt(new Date());
            property.setUpdateBy(user.getUsername());
            property.setUpdateAt(new Date());
            response = propertyRepository.save(property);
            user.setProperty(response);
            userRepository.save(user);
            // 2. add all permission for owner property
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.CHANNEL, user.getUsername()));
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.DASHBOARD, user.getUsername()));
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.INVENTORY, user.getUsername()));
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.REPORT, user.getUsername()));
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.RESERVATION, user.getUsername()));
            permissionRepository.save(new Permission(user.getEmail(), property, PermissionEnum.ROOM_RATE, user.getUsername()));
        }
        return response;
    }

    public Property findPropertyOwner(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        Optional<Property> property = propertyRepository.findByOwnerUser(user);
        if(!property.isPresent()){
            throw new ConstraintViolationException("this user not have any owner property", null);
        }
        return property.get();
    }

    public void addNewUserInProperty(String[] permissions, String email, Principal principal){
        Property property = findPropertyOwner(principal);
        List<Permission> permissionEntity = permissionRepository.findByEmail(email);
        if(permissionEntity.size() > 0){
            throw new ConstraintViolationException("this email already have property, please try another email.", null);
        }
        for(String permission : permissions){
            permissionRepository.save(new Permission(email, property, PermissionEnum.valueOf(permission), principal.getName()));
        }
    }
}