package com.cm.channelmanageapi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.cm.channelmanageapi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleIsExistValidator implements ConstraintValidator<RoleIsExist, Integer> {

    @Autowired
    RoleRepository roleRepository;

    public void initialize(RoleIsExistValidator constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        try{
            return roleRepository.findById(value).isPresent();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}