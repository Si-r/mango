package com.cm.channelmanageapi.repository;

import java.util.Optional;
import com.cm.channelmanageapi.entity.Property;
import com.cm.channelmanageapi.entity.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends CrudRepository<Property, Long> {
    Optional<Property> findById(Integer id);
    Optional<Property> findByOwnerUser(User user);
    void deleteById(Integer id);
}