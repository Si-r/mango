package com.cm.channelmanageapi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.cm.channelmanageapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class EmailDuplicateValidator implements ConstraintValidator<EmailDuplicate, String> {

    @Autowired
    UserRepository userRepository;

    public void initialize(EmailDuplicate constraintAnnotation) {
        
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !userRepository.findByEmail(value).isPresent();
    }
}