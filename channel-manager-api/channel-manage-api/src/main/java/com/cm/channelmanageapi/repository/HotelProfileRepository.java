package com.cm.channelmanageapi.repository;

import com.cm.channelmanageapi.entity.HotelProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HotelProfileRepository extends CrudRepository<HotelProfile, Long> {
    Optional<HotelProfile> findById(Integer id);
    Optional<HotelProfile> findByName(String name);
    void deleteById(Integer id);
}