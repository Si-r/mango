package com.cm.channelmanageapi.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cm.channelmanageapi.utils.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

/**
 * https://www.javainuse.com/spring/boot-jwt
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    JwtHandling jwtHandling;

    @Autowired
    JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    Formatter format;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        final String requestTokenHeader = request.getHeader("Authorization");
        String jwtToken = null;
        if(requestTokenHeader == null){
            filterChain.doFilter(request, response);
            return;
        }
        
        if (requestTokenHeader.startsWith("Bearer ") && SecurityContextHolder.getContext().getAuthentication() == null) {
			jwtToken = requestTokenHeader.substring(7);
			try {
                Claims claims = jwtHandling.validateToken(jwtToken); //**to-do** need to return User Object
                String username = (String) claims.get("username");
                List<String> permission = (List<String>) claims.get("permission");
                List<SimpleGrantedAuthority> authorityList = new ArrayList<SimpleGrantedAuthority>();
                permission.forEach(p -> {
                    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(p);
                    authorityList.add(authority);
                });
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = 
                    new UsernamePasswordAuthenticationToken(username, null, authorityList);
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                filterChain.doFilter(request, response);
			} catch (IllegalArgumentException e) {
                sendFailResponse(response, "Unable to get JWT Token");
			} catch (ExpiredJwtException e) {
                sendFailResponse(response, "JWT Token has expired");
			} catch (Exception e) {
                sendFailResponse(response, e.getMessage());
            }
		} else {
            sendFailResponse(response, "JWT Token does not begin with Bearer String");
        }  
    }

    private void sendFailResponse(HttpServletResponse response, String message) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(format.getFailCustomMessageResponse(message));
    }



}